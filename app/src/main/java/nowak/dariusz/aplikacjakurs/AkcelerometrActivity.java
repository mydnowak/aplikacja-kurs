package nowak.dariusz.aplikacjakurs;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AkcelerometrActivity extends AppCompatActivity implements SensorEventListener {

    @BindView(R.id.txtPierwszy) TextView tekstDlaX;
    @BindView(R.id.txtDrugi) TextView tekstDlaY;
    @BindView(R.id.txtTrzeci) TextView tekstDlaZ;

    private SensorManager sensorManager;
    private Sensor mAccelrometr;
    private Sensor mTemp;

      final String TAG = "DarekApp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akcelerometr);

        ButterKnife.bind(this);


        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelrometr = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mTemp = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);

        //TODO Poprawcie ten blad nie wiem o co chodzi
//        String f = "73e";
//        int francja = Integer.valueOf(f);

    }

    @Override
    public void onResume(){
        super.onResume();
        // odpalenie nasluchiwana dotyczacych zmian
        sensorManager.registerListener(this, mAccelrometr, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, mTemp, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause(){
        super.onPause();
        // wyrejestorwanie nasluchiwana dotyczacych zmian
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] values = event.values;
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            float x = values[0];
            float y = values[1];
            float z = values[2];

            tekstDlaX.setText(String.valueOf(x));
            tekstDlaY.setText(String.valueOf(y));
            tekstDlaZ.setText(String.valueOf(z));

            Log.d(TAG, String.valueOf(x));
            Log.d(TAG, String.valueOf(y));
            Log.d(TAG, String.valueOf(z));

        }
        if(event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE){
            float x = event.values[0];
          //  Log.d(TAG, String.valueOf(x));
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
