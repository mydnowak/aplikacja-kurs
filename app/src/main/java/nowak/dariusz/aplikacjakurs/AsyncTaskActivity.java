package nowak.dariusz.aplikacjakurs;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AsyncTaskActivity extends AppCompatActivity {


    int liczba = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);
        ButterKnife.bind(this);

      //  tvAndek.setText(String.valueOf(liczba));

        progressBar.setProgress(0);

    }

    @BindView(R.id.textView5)
    TextView tvAndek;

    @OnClick(R.id.button)
    public void wykonajAsynctask(){
        new MojAsyncTask(tvAndek, AsyncTaskActivity.this).execute("Darek", "Przemek");
       // new ZmienIntegerAsyncTask().execute(liczba);
    }

    public class ZmienIntegerAsyncTask extends AsyncTask<Integer, Void, Integer>{

        @Override
        protected Integer doInBackground(Integer... params) {
            int liczbaProblem = params[0];
            liczbaProblem = liczbaProblem * 5;

            return liczbaProblem;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            liczba = integer;
            tvAndek.setText(String.valueOf(liczba));
        }
    }



    @OnClick(R.id.btnPending)
    public void otworzPendingIntent(){
            Intent intent = new Intent(this, PanstwaActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        try{
            pendingIntent.send();
        }catch(PendingIntent.CanceledException e){
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btnAlarm)
    public void zaalarmuj(){

        Intent i = new Intent(this, MojReceiver.class);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, i, 0);

        // utworznie alarmu

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 3000, pendingIntent);



    }

    @OnClick(R.id.btnTest)
    public void testujemyWatek(){
        new Thread(new Runnable() {
            @Override
            public void run() {

                // opcja 1
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        tvAndek.setText("DZIALAM ??? ");
//                    }
//                });

                // opcja 2
                tvAndek.post(new Runnable() {
                    @Override
                    public void run() {
                        tvAndek.setText("DZIALAM ??? ");
                    }
                });

            }
        }).start();
    }

    Handler handler;
    @OnClick(R.id.btnHandler)
    public void testujemyHandler(){
        // utworz handler
         handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                // odbierz wiadomosc wyslana z watku
                tvAndek.setText(msg.obj.toString());

            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                // 1. wylosuj
                Random random = new Random();
                 int wylosowanaLiczba = random.nextInt(100);
                // 2. uspij na 3 SEK!
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // 3. przekaz wiadomosc
                Message wiadomosc = new Message();
                wiadomosc.obj = wylosowanaLiczba;
                // wyslij do handlera
                handler.sendMessage(wiadomosc);
            }
        }).start();
    }

     static final int CZAS = 10000;
     int mProgres = 0;
    @OnClick(R.id.btnPostDelayed)
    public void przekazDalej(){

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mProgres++;
                    progressBar.setProgress(mProgres);
                    if(mProgres < 10){
                        handler.postDelayed(this, 1000);
                    }
                }
            }, 1000);



        new Thread(new Runnable() {
            @Override
            public void run() {
                tvAndek.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tvAndek.setText("KONIEC");
                    }
                }, CZAS);

            }
        }).start();


    }

    @BindView(R.id.progressBar)ProgressBar progressBar;
}
