package nowak.dariusz.aplikacjakurs;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZasobyActivity extends AppCompatActivity {

    MediaPlayer mp;

    @OnClick(R.id.btnPlay)
    public void odegrajDzwieK(){
     //   I dunno by grapes (c) copyright 2008 Licensed under a Creative Commons Attribution (3.0) license. http://dig.ccmixter.org/files/grapes/16626 Ft: J Lang, Morusque
        mp = MediaPlayer.create(this, R.raw.siren_noise);
        mp.start();

    }

    @BindView(R.id.imageViewZdjecie)
    ImageView zdjecie;

    @OnClick(R.id.btnBlink)
    public void blink(){
        Animation blink = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.blink);
        zdjecie.startAnimation(blink);

    }

    @OnClick(R.id.btnMove)
    public void move(){
        Animation move = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move);
        zdjecie.startAnimation(move);
    }

    @OnClick(R.id.btnRotate)
    public void rotate(){
        Animation rotate = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.rotate);
        zdjecie.startAnimation(rotate);
    }

    @OnClick(R.id.btnZoom)
    public void zoom(){
        Animation zoom = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoom);
        zdjecie.startAnimation(zoom);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zasoby);

        // butter knife zawsze po SetContentView !!
        ButterKnife.bind(this);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage("https://blog.zooplus.pl/wp-content/uploads/sites/8/2015/02/mops.jpg", zdjecie);


    }
}
