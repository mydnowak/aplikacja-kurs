package nowak.dariusz.aplikacjakurs;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.io.IOException;
import java.util.Timer;

/**
 * Created by dariusznowak on 11.06.2017.
 */

public class MusicService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;

    }

    MediaPlayer mp;

    @Override
    public void onCreate(){
        super.onCreate();

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        mp = new MediaPlayer();
        try {
            mp.setDataSource(getApplicationContext(), Uri.parse("android.resource://nowak.dariusz.aplikacjakurs/"+ R.raw.dunno));
        } catch (IOException e) {
            e.printStackTrace();
        }
        mp.setLooping(true);
        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer player) {
                player.start();
            }
        });
        mp.prepareAsync();


        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        mp.reset();
        mp.release();
    }


}
