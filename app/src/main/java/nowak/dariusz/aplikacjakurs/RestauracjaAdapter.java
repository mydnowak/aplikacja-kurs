package nowak.dariusz.aplikacjakurs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import nowak.dariusz.aplikacjakurs.db.DaoMaster;
import nowak.dariusz.aplikacjakurs.db.DaoSession;
import nowak.dariusz.aplikacjakurs.db.Koszyk;
import nowak.dariusz.aplikacjakurs.db.KoszykDao;
import nowak.dariusz.aplikacjakurs.db.Produkt;
import nowak.dariusz.aplikacjakurs.db.ProduktDao;
import nowak.dariusz.dane.ToDoItem;

/**
 * Created by dariusznowak on 06.07.2017.
 */

public class RestauracjaAdapter extends RecyclerView.Adapter<RestauracjaAdapter.MyViewHolder>{

    private List<Produkt> listaProduktow;
    Context context;

    public RestauracjaAdapter(List<Produkt> produktList, Context context){
        this.listaProduktow = produktList;
        this.context = context;
    }

    @Override
    public RestauracjaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wiersz_listy_restauracja, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RestauracjaAdapter.MyViewHolder holder, final int position) {

        final Produkt item = listaProduktow.get(position);

        holder.nazwaProduktu.setText(item.getNazwa());
        holder.cena.setText(String.valueOf(item.getCena())+ " zł");
        holder.dodajProdukt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // otwieramy polaczenie z baza
                DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(context, "users-db");
                Database db = pomocnik.getWritableDb();
                // tworzymy nowa sesje
                DaoSession daoSession = new DaoMaster(db).newSession();
                KoszykDao koszykDao = daoSession.getKoszykDao();

                // generujemy liste elementow Koszyk ktore maja ID = produktowi ID w ktorym jestesmy
                // w odopwiedzi dostnaiemy liste o ilosci elementow 0 lub 1
                List<Koszyk> listaKosz = koszykDao.queryBuilder().where(KoszykDao.Properties.ProduktId.eq(item.getId())).list();

                // jesli dostajemy liste rowna 0 to dodajemy do koszyka nowy produkt
                if(listaKosz.size() == 0){
                    // tworzymy nowy produkt
                    Koszyk koszyk = new Koszyk();
                    // ustawiamy mu ilosc 1
                    koszyk.setIlosc(1);
                    koszyk.setProduktId(item.getId());
                    koszykDao.insert(koszyk);

                }else{
                    // jesli lista jest rowna 1 to aktualizujemy ilosc danego produktu w koszyku
                    Koszyk koszykDoAktualizacji = listaKosz.get(0);
                    koszykDoAktualizacji.setIlosc(koszykDoAktualizacji.getIlosc()+1);
                    koszykDao.update(koszykDoAktualizacji);
                }
                NaszeMetody.ShowMessage("Zwiększono ilość w koszyku o 1");

                if(context instanceof RestauracjaActivity){
                    ((RestauracjaActivity) context).pokazSume();
                }


            }
        });

    }



    @Override
    public int getItemCount() {
        return listaProduktow.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView nazwaProduktu;
        public TextView cena;
        public ImageButton dodajProdukt;

        public MyViewHolder(View itemView){
            super(itemView);
            nazwaProduktu = (TextView) itemView.findViewById(R.id.txtProdukt);
            cena = (TextView) itemView.findViewById(R.id.txtCena);
            dodajProdukt = (ImageButton) itemView.findViewById(R.id.ibDodaj);
        }
    }


}
