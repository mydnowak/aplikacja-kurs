package nowak.dariusz.aplikacjakurs;

/**
 * Created by dariusznowak on 14.06.2017.
 */

public class Panstwo {

    public Panstwo(int flaga, String stolica, String nazwaPanstwa) {
        this.flaga = flaga;
        this.stolica = stolica;
        this.nazwaPanstwa = nazwaPanstwa;
    }

    public int getFlaga() {
        return flaga;
    }

    public void setFlaga(int flaga) {
        this.flaga = flaga;
    }

    int flaga;

    public String getStolica() {
        return stolica;
    }

    public void setStolica(String stolica) {
        this.stolica = stolica;
    }

    String stolica;

    public String getNazwaPanstwa() {
        return nazwaPanstwa;
    }

    public void setNazwaPanstwa(String nazwaPanstwa) {
        this.nazwaPanstwa = nazwaPanstwa;
    }

    String nazwaPanstwa;

}
