package nowak.dariusz.aplikacjakurs;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.QueryBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nowak.dariusz.aplikacjakurs.db.DaoMaster;
import nowak.dariusz.aplikacjakurs.db.DaoSession;
import nowak.dariusz.aplikacjakurs.db.Koszyk;
import nowak.dariusz.aplikacjakurs.db.KoszykDao;
import nowak.dariusz.aplikacjakurs.db.Produkt;
import nowak.dariusz.aplikacjakurs.db.ProduktDao;
import nowak.dariusz.dane.ToDoItem;
import nowak.dariusz.dane.ZapytaniaBazy;

public class RestauracjaActivity extends AppCompatActivity {

    @OnClick(R.id.floatingActionButton)
    public void dodaj(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(RestauracjaActivity.this);
        alertDialog.setTitle("Wpisz produkt");

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout.LayoutParams doTextu = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout v = new LinearLayout(this);
        v.setOrientation(LinearLayout.VERTICAL);
        v.setLayoutParams(lp);

        final EditText nowyText = new EditText(this);
        nowyText.setHint("Wpisz nazwę produktu");
        nowyText.setLayoutParams(doTextu);
        v.addView(nowyText);

        final EditText textCena = new EditText(this);
        textCena.setHint("Wpisz cenę");
        textCena.setLayoutParams(doTextu);
        v.addView(textCena);




        alertDialog.setView(v);

        alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // tworzymy nowy produkt
                Produkt pr = new Produkt();
                pr.setNazwa(nowyText.getText().toString());
                pr.setCena(Float.valueOf(textCena.getText().toString()));
                // wstawiamy produkt do bazy
                produktDao.insert(pr);

                odczytajDaneBaza();

                pa = new RestauracjaAdapter(listaProduktow, getApplicationContext());
                mojRecycler.setAdapter(pa);
            }
        });

        alertDialog.show();

    }

    @BindView(R.id.restaurantLayout)
    CoordinatorLayout restLay;

    RestauracjaAdapter pa;
    List<Produkt> listaProduktow;
    RecyclerView mojRecycler;

    private DaoSession daoSession;
    ProduktDao produktDao;
    KoszykDao koszykDao;

    public void odczytajDaneBaza(){
        // odczytujemy liste danych z bazy
        listaProduktow = produktDao.queryBuilder().list();
      //  listaProduktow = daoSession.getProduktDao().loadAll();
    }

    Toolbar toolbar;
    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restauracja);

        ButterKnife.bind(this);


        listaProduktow = new ArrayList<>();

        // otwieramy polaczenie z baza
        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(this, "users-db");
          db = pomocnik.getWritableDb();
        // tworzymy nowa sesje
        daoSession = new DaoMaster(db).newSession();
        // laczymy sie z produktDao
        produktDao = daoSession.getProduktDao();
        koszykDao = daoSession.getKoszykDao();


        odczytajDaneBaza();



        mojRecycler = (RecyclerView) findViewById(R.id.mojRVi);

        // wybieramy manager layoutu (dostepny jeszcze Grid i StabbedGrid
        LinearLayoutManager llm = new LinearLayoutManager(this);
        // ustawiamy Manager Layoutu
        mojRecycler.setLayoutManager(llm);

        //utworzneie odstepu pomiedzy elementami listy
        DividerItemDecoration podzielenie = new DividerItemDecoration(mojRecycler.getContext(), llm.getOrientation());
        mojRecycler.addItemDecoration(podzielenie);

        // ustwaiamy nasz wlasny adapter do recyclerview
        pa = new RestauracjaAdapter(listaProduktow, RestauracjaActivity.this);
        mojRecycler.setAdapter(pa);

          toolbar = (Toolbar) findViewById(R.id.mojToolbar);
        toolbar.inflateMenu(R.menu.menu_koszyk);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId() == R.id.idRestaurant){
                    Intent i = new Intent(getApplicationContext(), KoszykActivity.class);
                    startActivity(i);
                }
                if(item.getItemId() == R.id.idSend){
                    wyslijMailem();
                }
                if(item.getItemId() == R.id.idDownload){
                    pobierz();
                }
                return false;
            }
        });

    }

    List<Produkt> menu;
    public void wyslijMailem(){
         menu = produktDao.queryBuilder().list();
         sprawdzCzyJestDostep();
    }

    public void pobierz(){
      listaProduktow = odczytajPamiecZewnetrzna();
        pa = new RestauracjaAdapter(listaProduktow, RestauracjaActivity.this);
        mojRecycler.setAdapter(pa);
    }

    final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    public void sprawdzCzyJestDostep(){
        // sprawdzamy czy jest przyznay dostep
        if(android.support.v4.app.ActivityCompat.checkSelfPermission(RestauracjaActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){

            // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
            if (ActivityCompat.shouldShowRequestPermissionRationale(RestauracjaActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                // jesli dostep blokowal pokazujemy po co nam to potrzebne
                showExplanation("Potrzebujemy pozwolenia", "Chcemy zapisać w pamięci zewnętrznej menu",
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }else{
                // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                requestPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }else{
            // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
            // sendsms to wlasna funkcja do wysylania smsow opisana ponizej
            zapiszDoPamieciZewntrznej(menu);
        }
    }

    // funkcja ktora pokazuje okienko systemowe z prosba o dany kod
    private void requestPermissions(String permissionName, int permissionRequestCode){
        ActivityCompat.requestPermissions(this, new String[]{permissionName} , permissionRequestCode);
    }

    // wlasna funkcja ktora pokazuje okienko z wyjasnieniem prosby o dostep
    private void showExplanation(String title, String message, final String permission, final int permissionRequestCode){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                requestPermissions(permission, permissionRequestCode);
            }
        });
        builder.show();
    }

    // metoda wywolywana za kazdym razem gdy uzytkownik podejmie decyzje o dostepie
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // jesli uzytkownik dal anuluj to dlugosc listy bedzie pusta
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    zapiszDoPamieciZewntrznej(menu); // dostep przyznany - mozemy zrobic co chcemy
                    Log.d(NaszeMetody.LOG_TAG, "Dostęp przyznany");
                } else {
                    Log.d(NaszeMetody.LOG_TAG, "Dostęp nie przyznany");
                    //  dostep nie przyznany ! musimy obsluzyc ten problem w aplikacji
                    // ponizej dodatkowo sprawdzamy czy zaznaczyl never ask again
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(RestauracjaActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        if (!showRationale) {
                         Log.d(NaszeMetody.LOG_TAG, "Uzytkownik zaznaczyl never ask again");
                        }
                    }
                }
                return;
            }

            // za pomoca swticha mozna przejrzec czasmi wiele prosb
        }
    }


    final static String NAZWA_PLIKU = "menu.sda";

    public void zapiszDoPamieciZewntrznej(List<Produkt> produkty){
        String lokalizacjFolderu = Environment.getExternalStorageDirectory().toString();
//        File myDir = new File(root + "/moj_folder");
//        myDir.mkdirs();
        File plik = new File (lokalizacjFolderu, NAZWA_PLIKU);

        Gson gson = new Gson();
        String jsonProdukty = gson.toJson(produkty);



        try {
            FileOutputStream outputStream = new FileOutputStream(plik);
            outputStream.write(jsonProdukty.getBytes());
            outputStream.close();

            Snackbar.make(restLay, "Zapisane", Snackbar.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Snackbar.make(restLay, "Wystąpił błąd zapisu", Snackbar.LENGTH_SHORT).show();
        }

    }

    public List<Produkt> odczytajPamiecZewnetrzna(){

        File plik = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), NAZWA_PLIKU);

        StringBuilder sb = new StringBuilder();

        try {
            FileInputStream fis =  new FileInputStream(plik);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
             sb = new StringBuilder();
            String line;
            while((line = br.readLine()) != null){
                sb.append(line);
            }
            Snackbar.make(restLay, "Odczytane", Snackbar.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Snackbar.make(restLay, "Wystąpił błąd odczytu", Snackbar.LENGTH_SHORT).show();
        }

        Type type = new TypeToken<List<Produkt>>(){}.getType();

        List<Produkt> odczytaneProdukty;

        Gson gson = new Gson();
        odczytaneProdukty = gson.fromJson(sb.toString(), type);


        return odczytaneProdukty;
    }

    public void pokazSume(){
        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(this, "users-db");
        db = pomocnik.getWritableDb();
        // tworzymy nowa sesje
        daoSession = new DaoMaster(db).newSession();
        // laczymy sie z produktDao
        produktDao = daoSession.getProduktDao();
        koszykDao = daoSession.getKoszykDao();

        // pobieram  liste elementow z koszyka
        List<Koszyk> koszyk = koszykDao.queryBuilder().list();
        float suma = 0;
        // na kazdym elemencie koszyka robie obliczenia
        for(Koszyk item: koszyk){
            // pobieram sobie ilosc danego produktu w koszyku
            int ilosc = item.getIlosc();
            // sprawdzam cene tego produktu
            List<Produkt> listaProdukt = produktDao.queryBuilder().where(ProduktDao.Properties.Id.eq(item.getProduktId())).list();
            float cena = listaProdukt.get(0).getCena();
            // mnoze ilosc * cena  i dodaje do sumy koszyka
            suma = suma + ((float)ilosc*cena);
        }

        // uakatualnia toolbara o wartosc tej sumy
        toolbar.setTitle(String.valueOf(suma) + " zł");

    }
}
