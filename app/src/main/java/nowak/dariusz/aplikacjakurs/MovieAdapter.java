package nowak.dariusz.aplikacjakurs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import org.greenrobot.greendao.database.Database;

import java.util.List;

import nowak.dariusz.aplikacjakurs.db.DaoMaster;
import nowak.dariusz.aplikacjakurs.db.DaoSession;
import nowak.dariusz.aplikacjakurs.db.Koszyk;
import nowak.dariusz.aplikacjakurs.db.KoszykDao;
import nowak.dariusz.aplikacjakurs.db.Produkt;
import nowak.dariusz.dane.FilmModel;

/**
 * Created by dariusznowak on 18.07.2017.
 */




public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder>{

    private List<FilmModel> listaFilmow;
    Context context;

    public MovieAdapter(List<FilmModel> listaFilmow, Context context){
        this.listaFilmow = listaFilmow;
        this.context = context;
    }

    @Override
    public MovieAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wiersz_listy_filmow, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MovieAdapter.MyViewHolder holder, final int position) {

        final FilmModel item = listaFilmow.get(position);

        holder.nazwaFilm.setText(item.getTytul());
        holder.rokProdukcjiFilm.setText(item.getRokPowstania());
        holder.ocenaFilm.setRating(item.getOcena());

    }



    @Override
    public int getItemCount() {
        return listaFilmow.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView nazwaFilm;
        TextView rokProdukcjiFilm;
        RatingBar ocenaFilm;

        public MyViewHolder(View itemView){
            super(itemView);

            nazwaFilm = (TextView) itemView.findViewById(R.id.txtTytul);
            rokProdukcjiFilm = (TextView) itemView.findViewById(R.id.txtRokProdukcji);
            ocenaFilm = (RatingBar) itemView.findViewById(R.id.ratingBarFilm);


        }
    }


}
