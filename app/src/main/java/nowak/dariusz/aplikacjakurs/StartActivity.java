package nowak.dariusz.aplikacjakurs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartActivity extends AppCompatActivity {

    @OnClick(R.id.btnAsync)
    public void async(){
        Intent i = new Intent(this, AsyncTaskActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnRestaurant)
    public void restaurant(){
        Intent i = new Intent(this, RestauracjaActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnRecycler)
    public void idzRecycler(){
        Intent i = new Intent(this, RecyclerViewActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnLogs)
    public void logi(){
        Intent i = new Intent(this, CallLogsActivity.class);
        startActivity(i);
    }



    @OnClick(R.id.btnZdjecia)
    public void idzDoZdjecia(){
        Intent i = new Intent(this, ZdjecieActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnMusic)
    public void idzMusic(){
        Intent i = new Intent(this, MusicActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnSimpleList)
    public void simpleList(){
        Intent i = new Intent(this, SimpleListViewActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnLogowanie)
    public void logowanie(){
        Intent i = new Intent(this, LogowanieActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnRejestracja)
    public void rejestracja(){
        Intent i = new Intent(this, RegisterActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnGra)
    public void gra(){
        Intent i = new Intent(this, KamienPapierNozyceActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnSMS)
    public void sms(){
        Intent i = new Intent(this, SMSActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnScrooling)
    public void idzScrolluj(){
        Intent i = new Intent(this, ScrollingActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnPanstwa)
    public void panstwa(){
        Intent i = new Intent(this, PanstwaActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnTab)
    public void tab(){
        Intent i = new Intent(this, TabActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnBaza)
    public void baza(){
        Intent i = new Intent(this, BazaDanychActivity.class);
        startActivity(i);
    }


    @OnClick(R.id.btnToDo)
    public void toDo(){
        Intent i = new Intent(this, ToDoListActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnLoader)
    public void loader(){
        Intent i = new Intent(this, BazaFilmowLoaderActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnFrag)
    public void otworzFragi(){
        Intent i = new Intent(this, LekcjaFragmentyActivity.class);
        startActivity(i);
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);

        NaszeMetody.InicjalizacjaNaszeMetody(getApplicationContext());

        // konfiguracja  raz w calym projekcie, ale na poczatku
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

    }


}
