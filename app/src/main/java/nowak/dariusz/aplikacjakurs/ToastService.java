package nowak.dariusz.aplikacjakurs;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import static android.content.ContentValues.TAG;

/**
 * Created by dariusznowak on 08.06.2017.
 */

public class ToastService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

//    Toast toast;
//    Timer timer;
//    TimerTask timerTask;
//    int counter;
    final Handler handler = new Handler();
    private static int counter = 1;

//    private class MojTimerTask extends TimerTask{
//        @Override
//        public void run(){
//            counter++;
//            toast.setText("WITAM PO RAZ: "+ counter);
//            toast.show();
//        }
//    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

//
//        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
//        timer = new Timer();
//        timerTask = new MojTimerTask();
//        timer.scheduleAtFixedRate(timerTask, 1000, 5*1000);
//
//
//        return super.onStartCommand(intent, flags, startId);

            Toast.makeText(getApplicationContext(),"Numbers of toasts: 1",Toast.LENGTH_SHORT).show();
            Log.d(TAG, "ToastService: started");
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    String message = "Number of toasts: " + String.valueOf(++counter);
                    Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                    handler.postDelayed(this, 0);
                }
            }, 1000);
            return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
//        timer.purge();
//        timerTask.cancel();
        handler.removeCallbacksAndMessages(null);
    }
}
