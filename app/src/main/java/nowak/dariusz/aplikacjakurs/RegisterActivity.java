package nowak.dariusz.aplikacjakurs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.Switch;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {



    @BindView(R.id.etPassword)
    EditText etPass;

    @BindView(R.id.etLogin)
    EditText etLogin;

    @BindView(R.id.etPasswordWrapper)
    TextInputLayout etPassWrapper;

    @BindView(R.id.etLoginWrapper)
    TextInputLayout etLoginWrapper;

    @BindView(R.id.etImie)
    EditText etImie;

    @BindView(R.id.etNazwisko)
    EditText etNazwisko;

    @BindView(R.id.etImieWrapper)
    TextInputLayout etImieWrapper;

    @BindView(R.id.etNazwiskoWrapper)
    TextInputLayout etNazwiskoWrapper;

    @BindView(R.id.etKodPocztowy)
    EditText etKodPocztowy;

    @BindView(R.id.etNumerTelefonu)
    EditText etNumerTelefonu;

    @BindView(R.id.etKodPocztowyWrapper)
    TextInputLayout etKodPocztowyWrapper;

    @BindView(R.id.etNumerTelefonuWrapper)
    TextInputLayout etNumerTelefonuWrapper;

    @BindView(R.id.acPanstwa)
    AutoCompleteTextView acPanstwa;

    @BindView(R.id.acPanstwaWrapper)
    TextInputLayout acPanstwaWrapper;

    @BindView(R.id.btnRegister)
    Button btnRejestruj;

    @BindView(R.id.checkBox)
    CheckBox checkBox;

    @BindView(R.id.switch1)
    Switch switch1;

    @BindView(R.id.toggleButton)
    ToggleButton toogleButton;


    @OnClick(R.id.btnRegister)
    public void rejestruj(){

      boolean czyFormularzPoprawny = walidacjaFormularza();
        if(czyFormularzPoprawny){
            if(checkBox.isChecked() == false){
                AlertDialog.Builder  builder = new AlertDialog.Builder(RegisterActivity.this, android.R.style.Theme_Material_Dialog_Alert);

                builder.setTitle("Zaakceptuj regulamin")
                        .setMessage("Jest to konieczne, aby dokonać rejestracji")
                        .setPositiveButton("Rozumiem", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();

            }else{

                SharedPreferences sharedPreferences = getSharedPreferences("dnowak.loginy", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(getString(R.string.PREF_LOGIN), etLogin.getText().toString());
                editor.putString(getString(R.string.PREF_HASLO), etPass.getText().toString());
                editor.apply();

                SharedPreferences sharedPref = getSharedPreferences("dnowak.adresy", Context.MODE_PRIVATE);
                SharedPreferences.Editor editorek = sharedPref.edit();
                editorek.putString(getString(R.string.PREF_KOD), etKodPocztowy.getText().toString());
                editorek.putString(getString(R.string.PREF_PANSTWO), acPanstwa.getText().toString());
                editorek.apply();



                // sprawdzam czy ktos zaznaczyl switcha czy nie
                boolean czyZaznaczylMaterialyDodatkowe = switch1.isChecked();

                btnRejestruj.setText("Rejestracja prawidłowa");
                // przejscie do mojego konta
                Intent i = new Intent(RegisterActivity.this, MojeKontoActivity.class);
                // pierwszy sposob - przekazuje zmienne osobno
                i.putExtra("przekazuje_imie", etImie.getText().toString());
                i.putExtra("przekazane_nazwisko", nazwisko);
                i.putExtra("materialy_dodatkowe", czyZaznaczylMaterialyDodatkowe);
                // lub drugi sposob , tworze nowy obiekt i tylko jego przenosze
                Uzytkownik nowyGosc = new Uzytkownik(etImie.getText().toString(), nazwisko, czyZaznaczylMaterialyDodatkowe);
                i.putExtra("calyUzytkownik", nowyGosc);
                startActivity(i);
            }
        }



    }

    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        etKodPocztowy.addTextChangedListener(postalCodeTextWatcher);

        List<String> listaPanstw = Arrays.asList(getResources().getStringArray(R.array.panstwa));
          adapter  = new ArrayAdapter<>(RegisterActivity.this,
                android.R.layout.simple_dropdown_item_1line, listaPanstw);

        acPanstwa.setAdapter(adapter);


        acPanstwa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                String str = acPanstwa.getText().toString();

                ListAdapter listAdapter = acPanstwa.getAdapter();
                for(int i = 0; i < listAdapter.getCount(); i++) {
                    String temp = listAdapter.getItem(i).toString();
                    temp = temp.substring(0,s.length());

                    if(str.compareTo(temp) == 0) {
                        acPanstwaWrapper.setError(null);
                        return;
                    }
                }

                acPanstwaWrapper.setError("Zle wpisujesz");
            }
        });



    }

//    @OnClick(R.id.acPanstwa)
//    public void klikPanstwoAC(){
//        acPanstwa.setText(null);
//    }

    String nazwisko;

    public boolean walidacjaFormularza() {
        boolean formularzPoprawny = true;

        String email = etLogin.getText().toString();
        String pass = etPass.getText().toString();
        String imie = etImie.getText().toString();
        nazwisko = etNazwisko.getText().toString();
        String telefon = etNumerTelefonu.getText().toString();
        String kodPocztowy = etKodPocztowy.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etLoginWrapper.setError(getString(R.string.wpisz_mail));
            formularzPoprawny = false;
        } else {
            etLoginWrapper.setError(null);
        }
        if (pass.length() < 10) {
            etPassWrapper.setError(getString(R.string.krotk_haslo));
            formularzPoprawny = false;
        } else {
            etPassWrapper.setError(null);
        }
        if (imie.isEmpty()) {
            etImieWrapper.setError(getString(R.string.wpisz_imie));
            formularzPoprawny = false;
        } else {
            etImieWrapper.setError(null);
        }
        if (nazwisko.isEmpty()) {
            etNazwiskoWrapper.setError(getString(R.string.wpisz_imie));
            formularzPoprawny = false;
        } else {
            etNazwiskoWrapper.setError(null);
        }
        if(telefon.length() != 9){
            etNumerTelefonuWrapper.setError(getString(R.string.numer_tel));
            formularzPoprawny = false;
        } else {
            etNumerTelefonuWrapper.setError(null);
        }
        // kod pocztowy
        if(kodPocztowy.length() != 6){
            etKodPocztowyWrapper.setError(getString(R.string.wpisz_kod));
            formularzPoprawny = false;
        } else {
            etKodPocztowyWrapper.setError(null);
        }
        // panstwa
        if(acPanstwa.getText().toString().isEmpty()){
            acPanstwaWrapper.setError(getString(R.string.wpisz_kraj));
            formularzPoprawny = false;
        } else {
            acPanstwaWrapper.setError(null);
        }




        return formularzPoprawny;
    }


    private final TextWatcher postalCodeTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            etKodPocztowyWrapper.setErrorEnabled(false);
            if (s.length()==3 && s.toString().charAt(s.length()-1) != '-') {
                String postalCodeValue = etKodPocztowyWrapper.getEditText().getText().toString();
                //charAt - pobira znak ze stringa
                //length - zwraca dlugosc
                String lastValue = String.valueOf(postalCodeValue.charAt(postalCodeValue.length()-1));
                String firstPart = postalCodeValue.substring(0,2);
                // substring mamy slowo SDA zwroci SD
                // czyli 0 - wiersz z ktorego startuje 2 na ktorym konczy ale go nie wlicza
                postalCodeValue = firstPart + "-" + lastValue;
                etKodPocztowyWrapper.getEditText().setText(postalCodeValue);
                // setSelection ustawia gdzie jest kursor
                etKodPocztowyWrapper.getEditText().setSelection(postalCodeValue.length());
            }
        }
    };


}
