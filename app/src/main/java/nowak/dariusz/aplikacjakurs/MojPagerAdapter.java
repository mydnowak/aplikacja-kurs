package nowak.dariusz.aplikacjakurs;

/**
 * Created by dariusznowak on 14.06.2017.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class MojPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public MojPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
            // zwraca na odpowiedniej pozycji taba odpowiedni tab
        switch (position) {
            case 0:
                return new MojTab1Fragment();
            case 1:
                return new MojTab2Fragment();
            case 2:
                return new MojTab3Fragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}