package nowak.dariusz.aplikacjakurs;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by dariusznowak on 25.07.2017.
 */


// pierwszy String - mowi jaki AsyncTask przyjmuje paremetry
    // drugi parametr mowi co przekazujemy do onProgressUpdate
// trzeci paramter mowi co zwraca doInBackgorund i co przyjmuje onPostExecute
public class MojAsyncTask extends AsyncTask<String, Integer, String> {

    TextView textView;
    AppCompatActivity activity;
    ProgressDialog dialog;

    public MojAsyncTask(TextView textView, AppCompatActivity activity){
        this.textView = textView;
        this.activity = activity;

        dialog = new ProgressDialog(activity);
        dialog.setIndeterminate(true);

    }


    // przed startem doInBackground
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        textView.setText("Zaczynamy!");

        dialog.setMessage("Proszę czekać");
        dialog.show();
    }

    // wykonanie zadania
    @Override
    protected String doInBackground(String... params) {
        // pobiera parametry
        String imie1 = params[0];
        String imie2 = params[1];


        for(int i = 1; i <=11; i+=1){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            publishProgress(i);
        }

        String imie = imie1 + " " + imie2;

        // przekazuje do onPostExecute
        return imie;
    }

    // wykonuje po wykonaniu doInBackground - uaktualniamy po zakonczeniu AsyncTaska UI
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        textView.setText("Cześć " +  result);

    }

    // uaktualniamy ekran podczas wykonywnaia doInBackground
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        textView.setText("Jest " + values[0] + " sek.");


        dialog.setMessage("Proszę czekać " + values[0] + " sek.");
    }



}
