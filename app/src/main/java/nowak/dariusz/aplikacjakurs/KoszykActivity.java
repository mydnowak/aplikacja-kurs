package nowak.dariusz.aplikacjakurs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import nowak.dariusz.aplikacjakurs.db.DaoMaster;
import nowak.dariusz.aplikacjakurs.db.DaoSession;
import nowak.dariusz.aplikacjakurs.db.Koszyk;
import nowak.dariusz.aplikacjakurs.db.KoszykDao;
import nowak.dariusz.aplikacjakurs.db.Produkt;
import nowak.dariusz.aplikacjakurs.db.ProduktDao;

public class KoszykActivity extends AppCompatActivity {

    KoszykAdapter pa;
    List<Koszyk> listKoszyk;
    RecyclerView mojRecycler;

    private DaoSession daoSession;
    KoszykDao koszykDao;

    public void odczytajDaneBaza(){
        // odczytujemy liste danych z bazy
        listKoszyk = koszykDao.queryBuilder().list();
    }

    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_koszyk);
        ButterKnife.bind(this);


        listKoszyk = new ArrayList<>();

        // otwieramy polaczenie z baza
        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(this, "users-db");
        db = pomocnik.getWritableDb();
        // tworzymy nowa sesje
        daoSession = new DaoMaster(db).newSession();
        // laczymy sie z produktDao
        koszykDao = daoSession.getKoszykDao();

        odczytajDaneBaza();

        mojRecycler = (RecyclerView) findViewById(R.id.mojRVi);

        // wybieramy manager layoutu (dostepny jeszcze Grid i StabbedGrid
        LinearLayoutManager llm = new LinearLayoutManager(this);
        // ustawiamy Manager Layoutu
        mojRecycler.setLayoutManager(llm);

        //utworzneie odstepu pomiedzy elementami listy
        DividerItemDecoration podzielenie = new DividerItemDecoration(mojRecycler.getContext(), llm.getOrientation());
        mojRecycler.addItemDecoration(podzielenie);

        // ustwaiamy nasz wlasny adapter do recyclerview
        pa = new KoszykAdapter(listKoszyk, KoszykActivity.this);
        mojRecycler.setAdapter(pa);



        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarKoszyk);
        // ustaw toolbar jako actionbar
        setSupportActionBar(myToolbar);

        // wykorzystanie actionbara do strzalki
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        pokazSume();

    }

    // aby utworzyc menu z pliku xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_act, menu);
        return true;
    }

    // aby przypisac akcje do elementow w menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mAFR:
                otworzActivityPoWynik();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    static final int WYSLIJ_SMS = 1;

    public void otworzActivityPoWynik(){
        Intent i = new Intent(this, SMSActivity.class);
        i.putExtra("suma", wyslijSuma);
        startActivityForResult(i, WYSLIJ_SMS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == WYSLIJ_SMS){
            if(resultCode == RESULT_OK){
                NaszeMetody.ShowMessage("WYSŁANO SMS");
            }else if(resultCode == RESULT_CANCELED){
                NaszeMetody.ShowMessage("NIE WYSŁANO SMS");

            }
        }
    }


    //   @BindView(R.id.mojTextView) TextView sumaText;

    String wyslijSuma;

    public void pokazSume(){
        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(this, "users-db");
        db = pomocnik.getWritableDb();
        // tworzymy nowa sesje
        daoSession = new DaoMaster(db).newSession();
        // laczymy sie z produktDao
      ProduktDao  produktDao = daoSession.getProduktDao();
        koszykDao = daoSession.getKoszykDao();

        // pobieram  liste elementow z koszyka
        List<Koszyk> koszyk = koszykDao.queryBuilder().list();

        float suma = 0;
        // na kazdym elemencie koszyka robie obliczenia
        for(Koszyk item: koszyk){
            // pobieram sobie ilosc danego produktu w koszyku
            int ilosc = item.getIlosc();
            // sprawdzam cene tego produktu
            List<Produkt> listaProdukt = produktDao.queryBuilder().where(ProduktDao.Properties.Id.eq(item.getProduktId())).list();
            float cena = listaProdukt.get(0).getCena();
            // mnoze ilosc * cena  i dodaje do sumy koszyka
            suma = suma + ((float)ilosc*cena);
        }

        wyslijSuma = String.valueOf(suma);

        // uakatualnia toolbara o wartosc tej sumy
    //    sumaText.setText(String.valueOf(suma) + " zł");
        getSupportActionBar().setTitle("Koszyk: " + String.valueOf(suma) + " zł");
    }
}
