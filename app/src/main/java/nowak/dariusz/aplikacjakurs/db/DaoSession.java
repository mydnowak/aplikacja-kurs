package nowak.dariusz.aplikacjakurs.db;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import nowak.dariusz.aplikacjakurs.db.Produkt;
import nowak.dariusz.aplikacjakurs.db.Koszyk;
import nowak.dariusz.aplikacjakurs.db.Zamowienia;
import nowak.dariusz.aplikacjakurs.db.SzczegolyZamowienia;

import nowak.dariusz.aplikacjakurs.db.ProduktDao;
import nowak.dariusz.aplikacjakurs.db.KoszykDao;
import nowak.dariusz.aplikacjakurs.db.ZamowieniaDao;
import nowak.dariusz.aplikacjakurs.db.SzczegolyZamowieniaDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig produktDaoConfig;
    private final DaoConfig koszykDaoConfig;
    private final DaoConfig zamowieniaDaoConfig;
    private final DaoConfig szczegolyZamowieniaDaoConfig;

    private final ProduktDao produktDao;
    private final KoszykDao koszykDao;
    private final ZamowieniaDao zamowieniaDao;
    private final SzczegolyZamowieniaDao szczegolyZamowieniaDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        produktDaoConfig = daoConfigMap.get(ProduktDao.class).clone();
        produktDaoConfig.initIdentityScope(type);

        koszykDaoConfig = daoConfigMap.get(KoszykDao.class).clone();
        koszykDaoConfig.initIdentityScope(type);

        zamowieniaDaoConfig = daoConfigMap.get(ZamowieniaDao.class).clone();
        zamowieniaDaoConfig.initIdentityScope(type);

        szczegolyZamowieniaDaoConfig = daoConfigMap.get(SzczegolyZamowieniaDao.class).clone();
        szczegolyZamowieniaDaoConfig.initIdentityScope(type);

        produktDao = new ProduktDao(produktDaoConfig, this);
        koszykDao = new KoszykDao(koszykDaoConfig, this);
        zamowieniaDao = new ZamowieniaDao(zamowieniaDaoConfig, this);
        szczegolyZamowieniaDao = new SzczegolyZamowieniaDao(szczegolyZamowieniaDaoConfig, this);

        registerDao(Produkt.class, produktDao);
        registerDao(Koszyk.class, koszykDao);
        registerDao(Zamowienia.class, zamowieniaDao);
        registerDao(SzczegolyZamowienia.class, szczegolyZamowieniaDao);
    }
    
    public void clear() {
        produktDaoConfig.clearIdentityScope();
        koszykDaoConfig.clearIdentityScope();
        zamowieniaDaoConfig.clearIdentityScope();
        szczegolyZamowieniaDaoConfig.clearIdentityScope();
    }

    public ProduktDao getProduktDao() {
        return produktDao;
    }

    public KoszykDao getKoszykDao() {
        return koszykDao;
    }

    public ZamowieniaDao getZamowieniaDao() {
        return zamowieniaDao;
    }

    public SzczegolyZamowieniaDao getSzczegolyZamowieniaDao() {
        return szczegolyZamowieniaDao;
    }

}
