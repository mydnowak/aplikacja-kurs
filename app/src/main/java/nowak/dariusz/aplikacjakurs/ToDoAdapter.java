package nowak.dariusz.aplikacjakurs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import nowak.dariusz.dane.ToDoItem;
import nowak.dariusz.dane.ZapytaniaBazy;

/**
 * Created by dariusznowak on 06.07.2017.
 */

public class ToDoAdapter extends RecyclerView.Adapter<ToDoAdapter.MyViewHolder>{

    private List<ToDoItem> listaZadan;
    ZapytaniaBazy polaczenieBazy;
    Context context;

    public ToDoAdapter(List<ToDoItem> zadaniaLista, Context context){
        this.listaZadan = zadaniaLista;

        polaczenieBazy = new ZapytaniaBazy(context);
        polaczenieBazy.otworzZapis();

        this.context = context;
    }

    @Override
    public ToDoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wiersz_listy_todo, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ToDoAdapter.MyViewHolder holder, final int position) {

        final ToDoItem item = listaZadan.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((onWyborZmianaListener)context).onZmianaListener(item.getOpisZadania());

            }
        });

        holder.trescZadania.setText(item.getNazwaZadania());
        holder.edytujZadanie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                alertDialog.setTitle("Wpisz zadanie do wykonania");

                LinearLayout lay = new LinearLayout(v.getContext());

                lay.setOrientation(LinearLayout.VERTICAL);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


                final EditText tekstGlowny = new EditText(v.getContext());
                tekstGlowny.setHint("Wpisz nazwe zadania");
                tekstGlowny.setLayoutParams(lp);
                tekstGlowny.setText(item.getNazwaZadania());
                final EditText opisZadania = new EditText(v.getContext());
                opisZadania.setHint("Wpisz opis zadania");
                opisZadania.setLayoutParams(lp);
                opisZadania.setText(item.getOpisZadania());
                lay.addView(tekstGlowny);
                lay.addView(opisZadania);
                alertDialog.setView(lay);

                alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        polaczenieBazy.edytujWierszToDo(tekstGlowny.getText().toString(), opisZadania.getText().toString(), String.valueOf(item.getId()));
                        listaZadan = polaczenieBazy.zwrocListeToDo();
                        notifyDataSetChanged();

                    }
                });

                alertDialog.show();

            }
        });
        holder.usunZadanie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                polaczenieBazy.usunWierszToDo(String.valueOf(item.getId()));
                listaZadan = polaczenieBazy.zwrocListeToDo();
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listaZadan.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView trescZadania;
        public ImageButton edytujZadanie;
        public ImageButton usunZadanie;

        public MyViewHolder(View itemView){
            super(itemView);
            trescZadania = (TextView) itemView.findViewById(R.id.txtNazwaZadania);
            edytujZadanie = (ImageButton) itemView.findViewById(R.id.ibEdytuj);
            usunZadanie = (ImageButton) itemView.findViewById(R.id.ibUsun);
        }
    }


}
