package nowak.dariusz.aplikacjakurs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nowak.dariusz.dane.FilmModel;
import nowak.dariusz.dane.ZapytaniaBazy;

public class BazaDanychActivity extends AppCompatActivity {

    ZapytaniaBazy polaczenieBazy;

    @BindView(R.id.wyswietlacz)
    TextView wyswietlacz;

    @BindView(R.id.movieRecyclerView)
    RecyclerView movieRecyclerView;


    @OnClick(R.id.btnDodaj)
    public void dodajDoBazy(){
        String zwrotIdWprowadzonego = "";
        // pro tip - ctrl + alt + v -> na koncu wiersza -> wsadzi nam na poczatku
          zwrotIdWprowadzonego += polaczenieBazy.dodajDoBazy("Wonder Woman", 70000, "2017", 4.5f);
          zwrotIdWprowadzonego += polaczenieBazy.dodajDoBazy("Batman", 70000, "2014", 4.5f);
          zwrotIdWprowadzonego += polaczenieBazy.dodajDoBazy("Avengers", 70000, "2016", 5.0f);
        zwrotIdWprowadzonego += polaczenieBazy.dodajDoBazy("Junior i Kraina Javy", 70000, "2013", 3.0f);
        zwrotIdWprowadzonego += polaczenieBazy.dodajDoBazy("Junior i przyjaciele", 5000, "2017", 2.0f);
        zwrotIdWprowadzonego += polaczenieBazy.dodajDoBazy("Przygody Seniora", 70000, "2017", 1.5f);
        wyswietlacz.setText(zwrotIdWprowadzonego);
    }

    @OnClick(R.id.btnWyswietl)
    public void wyswietlRekordy(){
        List<FilmModel> listaFilmow = polaczenieBazy.zwrocListeFilmow();

        // wybieramy manager layoutu (dostepny jeszcze Grid i StabbedGrid
        LinearLayoutManager llm = new LinearLayoutManager(this);
        // ustawiamy Manager Layoutu
        movieRecyclerView.setLayoutManager(llm);

        //utworzneie odstepu pomiedzy elementami listy
        DividerItemDecoration podzielenie = new DividerItemDecoration(movieRecyclerView.getContext(), llm.getOrientation());
        movieRecyclerView.addItemDecoration(podzielenie);

        // ustwaiamy nasz wlasny adapter do recyclerview
        MovieAdapter movieAdapter = new MovieAdapter(listaFilmow, BazaDanychActivity.this);
        movieRecyclerView.setAdapter(movieAdapter);
    }

    @OnClick(R.id.btnUsun)
    public void usun(){
        polaczenieBazy.usunWierszBaza("1");
        wyswietlRekordy();
    }

    @OnClick(R.id.btnEdytuj)
    public void edycja(){
        polaczenieBazy.edytujWiersz("2", 100);
        wyswietlRekordy();
    }

    @OnClick(R.id.btnCzysciel)
    public void czysciel(){
        polaczenieBazy.czyscielBazy();
        wyswietlRekordy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baza_danych);

        ButterKnife.bind(this);

        polaczenieBazy = new ZapytaniaBazy(BazaDanychActivity.this);
        polaczenieBazy.otworzZapis();



    }

    @Override
    protected void onDestroy(){
        polaczenieBazy.zamknij();
        super.onDestroy();
    }
}
