package nowak.dariusz.aplikacjakurs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ToDoSzczegolyFragment extends Fragment {


    public ToDoSzczegolyFragment() {
        // Required empty public constructor
    }

    TextView txtSzczegoly;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_to_do_szczegoly, container, false);
        txtSzczegoly = (TextView) v.findViewById(R.id.txtSzczegol);
        return v;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    public void ustawTekst(String tekst) {
        txtSzczegoly.setText(tekst);
    }


}