package nowak.dariusz.aplikacjakurs;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import nowak.dariusz.dane.FilmContentProvider;
import nowak.dariusz.dane.FilmyContract;

public class BazaFilmowLoaderActivity extends ListActivity implements
        LoaderManager.LoaderCallbacks<Cursor>{




    private static final int DELETE_ID = Menu.FIRST + 1;
    private SimpleCursorAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baza_filmow_loader);
        // ustawia na ListView odstep miedzy wierszami w pixelach
        this.getListView().setDividerHeight(2);
        fillData();

        // rejestruje dla jakiej kontrolki bedzie context menu
        registerForContextMenu(getListView());
    }


    // wybor elementu z menu kontekstowego
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case DELETE_ID:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                        .getMenuInfo();
                Uri uri = Uri.parse(FilmContentProvider.CONTENT_URI + "/"
                        + info.id);
                getContentResolver().delete(uri, null, null);
                fillData();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    // Opens the second activity if an entry is clicked
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        TextView txt = (TextView) v.findViewById(R.id.txtTytul);
        NaszeMetody.ShowMessage(txt.getText().toString());

    }



    private void fillData() {

         // ktore kolumny ma pobrac z bazy danych
        String[] from = new String[] {FilmyContract.FilmTabela.KOLUMNA_TYTUL, FilmyContract.FilmTabela.KOLUMNA_ROK_POWSTANIA};
        // Do ktorych pol z UI ma sie odniesc
        int[] to = new int[] { R.id.txtTytul, R.id.txtRokProdukcji};

        // inicjaca loadera,  pierwszy argument unikalne id po ktorym rozpoznamy w callback,
        // drugi argument to obiekt typu bundle ktory mozna przekazac
        // trzeci argument to activity ktore to implementuje
        getLoaderManager().initLoader(0, null, this);

        // simple cursor adapter polaczy nam kolumny z polami i wypelni nimi dane
        adapter = new SimpleCursorAdapter(this, R.layout.wiersz_lista_filmy_prosty, null, from,
                to, 0);

        setListAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, DELETE_ID, 0, "Usuń");
    }

    // po uzyciu initLoader laduje danymi
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = { FilmyContract.FilmTabela._ID, FilmyContract.FilmTabela.KOLUMNA_TYTUL, FilmyContract.FilmTabela.KOLUMNA_ROK_POWSTANIA};
        CursorLoader cursorLoader = new CursorLoader(this,
                FilmContentProvider.CONTENT_URI, projection, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // uaktualniamy tutaj UI po zakonczeniu odczytywania danych
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // dane nie sa dostepne
        adapter.swapCursor(null);
    }

}
