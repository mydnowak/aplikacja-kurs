package nowak.dariusz.aplikacjakurs;

import android.databinding.ObservableField;

import java.io.Serializable;

/**
 * Created by dariusznowak on 06.06.2017.
 */

public class Uzytkownik implements Serializable {


    public String imie;
    public String nazwisko;
    public boolean czyMaterialyDac;

   public Uzytkownik(String imie, String _nazwisko, Boolean _czyMaterialyDac) {
       this.imie = imie;
       nazwisko = _nazwisko;
       czyMaterialyDac = _czyMaterialyDac;
   }
}
