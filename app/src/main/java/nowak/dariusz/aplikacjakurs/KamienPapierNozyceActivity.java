package nowak.dariusz.aplikacjakurs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.w3c.dom.Text;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nowak.dariusz.secretphoto.PhotoSecretReturner;

public class KamienPapierNozyceActivity extends AppCompatActivity {

    //  aby latwiej bylo porownywac ze soba, przypisuje do wartosci int nazwy wlasne
    // dzieki czemu moge sie nimi w ten sposob poslugiwac
    static final int PAPIER = 1;
    static final int KAMIEN = 2;
    static final int NOZYCE = 3;

    // zmienne w ktorych przechowuje wynik
    int wynikMoj;
    int wynikKomputera;
    // zdjecia
    int zdjecieMoj;
    int zdjecieKomputer;

    // cofnij przycisk
    boolean cofnij = false;

    @BindView(R.id.glowny_layout)
    RelativeLayout glownyLayout;
    @BindView(R.id.losMoj)ImageView losMoj;
    @BindView(R.id.losKomputer)ImageView losKomputer;
    @BindView(R.id.txtWynikJa)TextView txtJa;
    @BindView(R.id.txtWynikKomputer)TextView txtKomputer;
    @OnClick(R.id.btnKamien)
    public void uderzKamien()
    {
        // jesli wybieram kamien wywoluje funkcje ustawienia wyniku z argumentem kamien
        ustawWynik(KAMIEN);
        // takze ustawiam obrazek kamienia w miejscu gdzie pokazuje moj wybor
        // w tym celu wykorzystuje zainicjanowany wczesniej ImageLoader
        imageLoader.displayImage(returnImageURI(R.drawable.and_stone), losMoj);
        zdjecieMoj = R.drawable.and_stone;
    }

    @OnClick(R.id.btnNozyce)
    public void uderzNozyce()
    {
        ustawWynik(NOZYCE);
        imageLoader.displayImage(returnImageURI(R.drawable.and_scissors), losMoj);
        zdjecieMoj = R.drawable.and_scissors;
    }

    public String returnImageURI(int link){
        // aby otworzyc z resource bitmapy za pomoca ImageLoadera korzystam z wlasnej funkcji
        // ktora do nazwy zasobow dodaje przedrostek drawable://
        String imageUri = "drawable://" + link;
        return imageUri;
    }

    @OnClick(R.id.btnPapier)
    public void uderzPapier()
    {
        ustawWynik(PAPIER);
        imageLoader.displayImage(returnImageURI(R.drawable.and_papier), losMoj);
        zdjecieMoj = R.drawable.and_papier;
    }

    public int wylosujCoGraKomputer(){
        // ta funkcja losuje co wybiera komputer
        Random rand = new Random();
        int n = rand.nextInt(3)+1;
        return n;
    }

    public int zwrocWylosowanyDoUstawienia(int losKomputer){
        // ta funkcja zwraca odniesienie do zasobow w zaleznosci od tego co wybierze komputer
        if(losKomputer == PAPIER){
            return R.drawable.and_papier;
        }
        if(losKomputer == KAMIEN){
            return R.drawable.and_stone;
        }
        if(losKomputer == NOZYCE){
            return R.drawable.and_scissors;
        }
        return 0;

    }

    static String WYNIKOMPUTERA = "WYNIKKOMPUTERA";

    static String OBRAZEK_MOJ = "obraz_moj";
    static String OBRAZEK_KOMPUTERA = "obraz_komp";
    static String COFNIECIE = "cofniecie";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // aby zachowac punkty
        outState.putInt("WYNIKMOJ", wynikMoj);
        outState.putInt(WYNIKOMPUTERA, wynikKomputera);
        // aby zachowac obrazkow
        outState.putInt(OBRAZEK_MOJ, zdjecieMoj);
        outState.putInt(OBRAZEK_KOMPUTERA, zdjecieKomputer);

        // czy wykorzystal swoje cofniecie
        outState.putBoolean(COFNIECIE, cofnij);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // odbieram dane
        int wynikM = savedInstanceState.getInt("WYNIKMOJ");
        int wynikK = savedInstanceState.getInt(WYNIKOMPUTERA);
        // przypisuje do aktualnych wynikow
        wynikMoj = wynikM;
        wynikKomputera = wynikK;

        // przypisuje wyniki do tekstu aby uzytkownik je widzial
        txtJa.setText(String.valueOf(wynikM));
        txtKomputer.setText(String.valueOf(wynikK));

        //odbieram obazki
        int obrazekMoj = savedInstanceState.getInt(OBRAZEK_MOJ);
        int obrazekKomputera = savedInstanceState.getInt(OBRAZEK_KOMPUTERA);

        // pobiera cofniecie
        cofnij = savedInstanceState.getBoolean(COFNIECIE);

        // ustawiam obrazki
        imageLoader.displayImage(returnImageURI(obrazekMoj), losMoj);

        imageLoader.displayImage(returnImageURI(obrazekKomputera), losKomputer);

    }

    static final int DLA_MNIE = 1;
    static final int DLA_KOMPA = 2;
    static final int NA_REMIS = 3;

    public void ustawWynik(int klikniete){
        int komputerGra = wylosujCoGraKomputer();
        zdjecieKomputer = zwrocWylosowanyDoUstawienia(komputerGra);
        imageLoader.displayImage(returnImageURI(zdjecieKomputer), losKomputer);

        // ponizej przedstawione wszystkie zaleznosci
        if(klikniete == PAPIER){
            if(komputerGra == PAPIER){
                showSnackbar(R.string.remis, R.color.yellow, NA_REMIS);
            }else if(komputerGra == NOZYCE){
                showSnackbar(R.string.punkt_komp, R.color.red, DLA_KOMPA);
                // jezeli pkt zdobywa kopmputer to zwiekszam jego wynik o 1
                wynikKomputera++;
            }else if(komputerGra == KAMIEN){
                showSnackbar(R.string.punkt_me, R.color.green, DLA_MNIE);
                // jezeli pkt zdobywam ja to zwiekszam moj wynik o 1
                wynikMoj++;
            }
        }else if(klikniete == NOZYCE){
            if(komputerGra == PAPIER){
                showSnackbar(R.string.punkt_me, R.color.green, DLA_MNIE);
                wynikMoj++;
            }else if(komputerGra == NOZYCE){
                showSnackbar(R.string.remis, R.color.yellow, NA_REMIS);
            }else if(komputerGra == KAMIEN){
                showSnackbar(R.string.punkt_komp, R.color.red, DLA_KOMPA);
                wynikKomputera++;
            }
        } else if(klikniete == KAMIEN){
            if(komputerGra == PAPIER){
                showSnackbar(R.string.punkt_komp, R.color.red, DLA_KOMPA);
                wynikKomputera++;
            }else if(komputerGra == NOZYCE){
                showSnackbar(R.string.punkt_me, R.color.green, DLA_MNIE);
                wynikMoj++;
            }else if(komputerGra == KAMIEN){
                showSnackbar(R.string.remis, R.color.yellow, NA_REMIS);

            }
        }


        uaktualnijWyniki();
        sprawdzCzyKoniec();
    }




    public void showSnackbar(int msg, int color, final int dlakogo){
        Snackbar mySnackbar = Snackbar.make(glownyLayout,  getString(msg), Snackbar.LENGTH_LONG);
        View view = mySnackbar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(ContextCompat.getColor(getApplicationContext(), color));
        if(cofnij == false){
            mySnackbar.setAction("COFNIJ", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cofnij = true;
                    if(dlakogo == NA_REMIS){
                        NaszeMetody.ShowMessage("Ale straciłeś głupio swoj ruch");
                    }else if(dlakogo == DLA_MNIE){
                        NaszeMetody.ShowMessage("Przeciez to Ty zdobyles punkt!");
                        wynikMoj--;
                    }else if(dlakogo == DLA_KOMPA){
                        NaszeMetody.ShowMessage("Komputer stracił swój punkt");
                        wynikKomputera--;
                    }
                    uaktualnijWyniki();
                }
            });
        }

        mySnackbar.show();
    }

    public void uaktualnijWyniki(){
        // ustawiam tekst z wynikami
        txtJa.setText(String.valueOf(wynikMoj));
        txtKomputer.setText(String.valueOf(wynikKomputera));
    }

    public void sprawdzCzyKoniec(){
        // sprawdzam czy ktos z nas osiagnal wynik = 5 bo jesli tak to oznacza to koniec gry
        if(wynikMoj == 5 || wynikKomputera == 5){
            AlertDialog.Builder  builder = new AlertDialog.Builder(KamienPapierNozyceActivity.this, android.R.style.Theme_Material_Dialog_Alert);

            builder.setTitle("Koniec gry")
                    .setMessage("Czy chcesz zagrać jeszcze raz?")
                    .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            resetujGre();
                        }
                    })
                    .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            resetujGre();
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    ImageLoader imageLoader;


    // bitBucsket nasluchuje s
    public void resetujGre(){
        // resetuje gre czyli ustawiam wyswietalny wynik na 0
        // oraz zmienne ktore przechowuja wynik na 0
        txtJa.setText("");
        txtKomputer.setText("");
        wynikKomputera = 0;
        wynikMoj = 0;
        cofnij = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kamien_papier_nozyce);
        ButterKnife.bind(this);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        // utworzenie instancji
        imageLoader = ImageLoader.getInstance();



    }
}
