package nowak.dariusz.aplikacjakurs;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import nowak.dariusz.aplikacjakurs.databinding.ActivityMojeKontoBinding;

public class MojeKontoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMojeKontoBinding binding  = DataBindingUtil.setContentView(this, R.layout.activity_moje_konto);

        // albo odbieramy wszystkie pola z osobna
        // i przypisujemy je do nowego obiektu uzytkownika
        String imie = getIntent().getStringExtra("przekazuje_imie");
        String nazwisko = getIntent().getStringExtra("przekazane_nazwisko");
        Boolean matDodatkowe = getIntent().getBooleanExtra("materialy_dodatkowe", false);
        Uzytkownik nowyGosc = new Uzytkownik(imie, nazwisko, matDodatkowe);


        // albo odbieram przekazany obiekt klasy Uzytkownik
        Uzytkownik przekazanyGosc = (Uzytkownik) getIntent().getSerializableExtra("calyUzytkownik");

        // ladujemy usera do klasy binding
        binding.setUzytkownik(przekazanyGosc);

    }


}
