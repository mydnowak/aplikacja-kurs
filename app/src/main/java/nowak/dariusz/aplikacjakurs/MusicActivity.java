package nowak.dariusz.aplikacjakurs;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MusicActivity extends AppCompatActivity {



    @OnClick(R.id.btnMusicStart)
    public void start(){
        if(isConnected(getApplicationContext()) == true){
            sharedPreferences.edit().putBoolean(MUZYKA, true).apply();
            Intent i = new Intent(this, MusicService.class);
            startService(i);

        }

    }

    @OnClick(R.id.btnMusicStop)
    public void stop(){
        sharedPreferences.edit().putBoolean(MUZYKA, false).apply();
        Intent i = new Intent(this, MusicService.class);
        stopService(i);

    }

    public static boolean isConnected(Context context) {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        return plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB;
    }
    public static final String MY_PREFS = "MOJ_KURSIK";
    public static final String MUZYKA = "kursMusicKey";
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        // ustaw toolbar jako actionbar
        setSupportActionBar(myToolbar);

        // wykorzystanie actionbara do strzalki
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


    }


    // aby utworzyc menu z pliku xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.moj_menu, menu);
        return true;
    }

    // aby przypisac akcje do elementow w menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.bateryMenu:
                sprawdzLadowanie();
                return true;
            case R.id.infoMenu:
                pokazInfo();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // wlasna funkcja ktora wyswietla tekst w zaleznosci od tego czy jest ladowanie
    public void sprawdzLadowanie(){
        if(isConnected(getApplicationContext())){
            NaszeMetody.ShowMessage("Ładuje");
        }else{
            NaszeMetody.ShowMessage("Nie ładuje");
        }
    }

    // pokazuje info
    public void pokazInfo(){
        NaszeMetody.ShowMessage("Ta piosenka powstała w 2002 roku");
    }


}
