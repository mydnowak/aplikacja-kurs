package nowak.dariusz.aplikacjakurs;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import nowak.dariusz.aplikacjakurs.db.DaoMaster;
import nowak.dariusz.aplikacjakurs.db.DaoSession;
import nowak.dariusz.aplikacjakurs.db.Koszyk;
import nowak.dariusz.aplikacjakurs.db.KoszykDao;
import nowak.dariusz.aplikacjakurs.db.Koszyk;
import nowak.dariusz.aplikacjakurs.db.Produkt;
import nowak.dariusz.aplikacjakurs.db.ProduktDao;
import nowak.dariusz.dane.ToDoItem;

/**
 * Created by dariusznowak on 06.07.2017.
 */

public class KoszykAdapter extends RecyclerView.Adapter<KoszykAdapter.MyViewHolder>{

    private List<Koszyk> listaProduktow;
    Context context;

    public KoszykAdapter(List<Koszyk> produktList, Context context){
        this.listaProduktow = produktList;
        this.context = context;
    }

    @Override
    public KoszykAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wiersz_listy_koszyk, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final KoszykAdapter.MyViewHolder holder, final int position) {

        final Koszyk item = listaProduktow.get(position);

        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(context, "users-db");
        Database db = pomocnik.getWritableDb();
        // tworzymy nowa sesje
        DaoSession daoSession = new DaoMaster(db).newSession();
       final KoszykDao koszykDao = daoSession.getKoszykDao();
        ProduktDao produktDao = daoSession.getProduktDao();

        List<Produkt> listPr = produktDao.queryBuilder().where(ProduktDao.Properties.Id.eq(item.getProduktId())).list();
        Produkt pr = listPr.get(0);

        holder.nazwaProduktu.setText(pr.getNazwa());
        holder.cena.setText("Cena: " + String.valueOf(pr.getCena() + " zł"));
        holder.ilosc.setText(zwrocIlosc(item.getIlosc()));
        holder.dodajProdukt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // otwieramy polaczenie z baza

                    // jesli lista jest rowna 1 to aktualizujemy ilosc danego produktu w koszyku

                    item.setIlosc(item.getIlosc()+1);
                    koszykDao.update(item);

                holder.ilosc.setText(zwrocIlosc(item.getIlosc()));

                if(context instanceof KoszykActivity){
                    ((KoszykActivity) context).pokazSume();
                }


            }
        });

        holder.usunProdukt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                item.setIlosc(item.getIlosc()-1);
                koszykDao.update(item);

                holder.ilosc.setText(zwrocIlosc(item.getIlosc()));

                if(context instanceof KoszykActivity){
                    ((KoszykActivity) context).pokazSume();
                }


            }
        });

    }

    public String zwrocIlosc(int ilosc){
        return "Ilość: " + String.valueOf(ilosc);
    }



    @Override
    public int getItemCount() {
        return listaProduktow.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView nazwaProduktu;
        public TextView cena;
        public ImageButton dodajProdukt;
        public ImageButton usunProdukt;
        public TextView ilosc;

        public MyViewHolder(View itemView){
            super(itemView);
            nazwaProduktu = (TextView) itemView.findViewById(R.id.txtProdukt);
            cena = (TextView) itemView.findViewById(R.id.txtCena);
            dodajProdukt = (ImageButton) itemView.findViewById(R.id.ibDodaj);
            usunProdukt = (ImageButton) itemView.findViewById(R.id.ibUsun);
            ilosc = (TextView) itemView.findViewById(R.id.txtIlosc);
        }
    }


}
