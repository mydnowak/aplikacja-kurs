package nowak.dariusz.aplikacjakurs;


import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import nowak.dariusz.secretphoto.PhotoSecretReturner;


/**
 * A simple {@link Fragment} subclass.
 */
public class MojTab2Fragment extends Fragment {


    public MojTab2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_moj_tab2, container, false);
    ImageView zdj = (ImageView) v.findViewById(R.id.imageView2);
        // utworzenie instancji
        ImageLoader imageLoader = ImageLoader.getInstance();
        String url = "http://android.com.pl/images/user-images/lwr/2016/09/AndroidPIT-Android-Nougat-9734-w782.jpg";
        imageLoader.displayImage(url, zdj);
        return v;
    }

}
