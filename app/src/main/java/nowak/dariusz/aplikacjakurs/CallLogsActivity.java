package nowak.dariusz.aplikacjakurs;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CallLogsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_logs);
        ButterKnife.bind(this);

        sprawdzCzyJestDostep();


    }

    @BindView(R.id.tvCallLogs) TextView txtCallLogs;

    private String getCallDetails() {

        StringBuffer sb = new StringBuffer();

        // deklaruje ze bedzie odpytywac content providera ktory dostarcza informacje o rozmowach telefonu
        Uri allCalls = Uri.parse("content://call_log/calls");
        // cursor odpytuje content providera o URI ktorym mu dostarczylismy
        Cursor managedCursor = getContentResolver().query(allCalls, null, null, null, CallLog.Calls.DATE + " DESC");

        // pobranie indeksu konkretnej kolumny
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

        sb.append("Call Details :");

        // kursor bedzie sprawdal kazdy wiersz
        while (managedCursor.moveToNext()) {

            // w danym wierszu pobiera kolumne o danym indexie
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
                    + dir + " \nCall Date:--- " + callDayTime
                    + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
        }
        managedCursor.close();
        return sb.toString();

    }

    final int MY_PERMISSIONS_REQUEST_READ_LOGS = 1;

    public void sprawdzCzyJestDostep(){
        // sprawdzamy czy jest przyznay dostep
        if(android.support.v4.app.ActivityCompat.checkSelfPermission(CallLogsActivity.this, Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED){

            // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
            if (ActivityCompat.shouldShowRequestPermissionRationale(CallLogsActivity.this,
                    Manifest.permission.READ_CALL_LOG)){
                // jesli dostep blokowal pokazujemy po co nam to potrzebne
                showExplanation("Potrzebujemy pozwolenia", "Chcemy odczytać połaczenia w telefonie",
                        Manifest.permission.READ_CALL_LOG, MY_PERMISSIONS_REQUEST_READ_LOGS);
            }else{
                // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                requestPermissions(Manifest.permission.READ_CALL_LOG, MY_PERMISSIONS_REQUEST_READ_LOGS);
            }
        }else{
            // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
            // sendsms to wlasna funkcja do wysylania smsow opisana ponizej
            txtCallLogs.setText(getCallDetails());
        }
    }

    // funkcja ktora pokazuje okienko systemowe z prosba o dany kod
    private void requestPermissions(String permissionName, int permissionRequestCode){
        ActivityCompat.requestPermissions(this, new String[]{permissionName} , permissionRequestCode);
    }

    // wlasna funkcja ktora pokazuje okienko z wyjasnieniem prosby o dostep
    private void showExplanation(String title, String message, final String permission, final int permissionRequestCode){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                requestPermissions(permission, permissionRequestCode);
            }
        });
        builder.show();
    }

    // metoda wywolywana za kazdym razem gdy uzytkownik podejmie decyzje o dostepie
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_LOGS: {
                // jesli uzytkownik dal anuluj to dlugosc listy bedzie pusta
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    txtCallLogs.setText(getCallDetails()); // dostep przyznany - mozemy zrobic co chcemy
                    Log.d(NaszeMetody.LOG_TAG, "Dostęp przyznany");
                } else {
                    Log.d(NaszeMetody.LOG_TAG, "Dostęp nie przyznany");
                    //  dostep nie przyznany ! musimy obsluzyc ten problem w aplikacji
                    // ponizej dodatkowo sprawdzamy czy zaznaczyl never ask again
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(CallLogsActivity.this,
                                Manifest.permission.READ_CALL_LOG);
                        if (!showRationale) {
                            Log.d(NaszeMetody.LOG_TAG, "Uzytkownik zaznaczyl never ask again");
                        }
                    }
                }
                return;
            }

            // za pomoca swticha mozna przejrzec czasmi wiele prosb
        }
    }
}
