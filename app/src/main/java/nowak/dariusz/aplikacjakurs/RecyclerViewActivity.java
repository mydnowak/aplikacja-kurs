package nowak.dariusz.aplikacjakurs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);


        // tworzymy liste zawierajaca obiekty Panstwo
        List<Panstwo> listaPanstw = new ArrayList<>();

        // tworzymy nowe obiekty typu Panstwo
        Panstwo polska = new Panstwo(R.drawable.pl, "Warszawa", "Polska");
        Panstwo brazylia = new Panstwo(R.drawable.br, "Brasilia", "Brazylia");
        Panstwo japonia = new Panstwo(R.drawable.jp, "Tokyo", "Japonia");
        Panstwo wielkaBrytania = new Panstwo(R.drawable.gb, "Londyn", "Wielka Brytania");
        Panstwo kanada = new Panstwo(R.drawable.ca, "Ottawa", "Kanada");
        Panstwo nowaZelandia = new Panstwo(R.drawable.au, "Nowa Zelandia", "Canberra");


        // dodajemy obiekty do listy
        listaPanstw.add(polska);
        listaPanstw.add(brazylia);
        listaPanstw.add(japonia);
        listaPanstw.add(wielkaBrytania);
        listaPanstw.add(kanada);
        listaPanstw.add(nowaZelandia);

        // odnajdujemy recyclerview po id z XML
        RecyclerView mojRecycler = (RecyclerView) findViewById(R.id.mojRecyclerView);

        // wybieramy manager layoutu (dostepny jeszcze Grid i StabbedGrid
        LinearLayoutManager llm = new LinearLayoutManager(this);
        // ustawiamy Manager Layoutu
        mojRecycler.setLayoutManager(llm);

        // ustwaiamy nasz wlasny adapter do recyclerview
        PanstwaAdapter pa = new PanstwaAdapter(listaPanstw);
        mojRecycler.setAdapter(pa);


    }
}
