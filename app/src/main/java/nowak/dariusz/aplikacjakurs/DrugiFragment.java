package nowak.dariusz.aplikacjakurs;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class DrugiFragment extends Fragment {


    public DrugiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_drugi, container, false);

        Button przycisk = (Button) v.findViewById(R.id.btnPokaztekst);

        przycisk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NaszeMetody.ShowMessage(wiadomosc);
            }
        });

        Button przycisk_przeslij_dalej = (Button) v.findViewById(R.id.btnPrzeslijDalej);
        przycisk_przeslij_dalej.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.przeslijText("SUPER TAJNY KOD TO: WEE_22_BIPP_33");
            }
        });

        return v;
    }

    PrzesylanyTekst mCallback;

    // nasz interfejs ktory musi implemetnowa activity ktore implementuje
    public interface PrzesylanyTekst{
         void przeslijText(String text);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // zeby sie nie krzyczalo o deprecated
        Activity activity = getActivity();

        // upewniamy sie ze kontener implemetuje ten interfejs
        // w innym wypadku wyrzucamy blad
        try {
            mCallback = (PrzesylanyTekst) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " musi implementowac PrzesylanyTekst");
        }
    }
    String wiadomosc;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bunle = getArguments();
        if(bunle != null){
            wiadomosc = bunle.getString("tajne");
        }
    }

    // chronimy przed ewentualnymi wyicekami pamieci
    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }
}
