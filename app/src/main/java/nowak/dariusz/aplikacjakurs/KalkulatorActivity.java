package nowak.dariusz.aplikacjakurs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class KalkulatorActivity extends AppCompatActivity {

    @BindView(R.id.dana1)
    EditText dana1;

    @BindView(R.id.dana2)
    EditText dana2;

    @BindView(R.id.dana3)
    EditText dana3;

    @BindView(R.id.txtWynik)
    TextView txtWynik;


    @OnClick(R.id.btnOblicz)
    public void zrobObliczenia(){
        String dan1 = dana1.getText().toString();
        String dan2 = dana2.getText().toString();
        String dan3 = dana3.getText().toString();

        double przeliczonaDana1 = Double.parseDouble(dan1);
        double przeliczonaDana2 = Double.parseDouble(dan2);
        double przeliczonaDana3 = Double.parseDouble(dan3);

        double krok1 = Math.pow(przeliczonaDana1, 2);
        double krok2 = Math.pow(przeliczonaDana2, 2);

        double suma = krok1 + krok2;

        double pierwiastek = Math.sqrt(suma);

        double wynik = pierwiastek / przeliczonaDana3;

        txtWynik.setText(String.format("%.2f", wynik));



    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kalkulator);
        ButterKnife.bind(this);

        getApplicationContext();


    }
}
