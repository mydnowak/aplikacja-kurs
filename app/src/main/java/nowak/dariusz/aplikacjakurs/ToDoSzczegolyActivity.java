package nowak.dariusz.aplikacjakurs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class ToDoSzczegolyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_szczegoly);

        Intent i = getIntent();
        String szcz = i.getStringExtra("szcz");

        ToDoSzczegolyFragment  fr = (ToDoSzczegolyFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentSzcz);
        fr.ustawTekst(szcz);
    }
}
