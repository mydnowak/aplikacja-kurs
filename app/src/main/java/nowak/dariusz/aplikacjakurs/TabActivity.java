package nowak.dariusz.aplikacjakurs;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class TabActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        // ustawimy Toolbar jako ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // pamietamy rowniez zeby to activity mialo theme ktory dziedziczy po theme z noactionbar

        // ustawiamy ikonke cofniecia
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // aby dzialala pamietamy zeby ustwiamy parentactivity w AndroidManifest dla danego activity
        // lub nadpisujemy jej dzialanie tutaj


        // odnajdujemy nowy layout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        // tworzymy dynamicznie taby z ustawieniem ikony lub zdjecia lub obojga razem
        tabLayout.addTab(tabLayout.newTab().setText("Opis"));
        tabLayout.addTab(tabLayout.newTab().setText("Zdjęcie"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_action_info));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // odnjadujemy viewpager z xml
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        // tworzymy z naszego wlasnego adaptera gdzie przekazujemy liczbe tabow
        final MojPagerAdapter adapter = new MojPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        // laczymy adapter z viewapager
        viewPager.setAdapter(adapter);

        // nasluchuje zmiany tabow
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        // nasluchuje klikniecia w taby
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


}
