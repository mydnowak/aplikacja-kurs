package nowak.dariusz.aplikacjakurs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SerwisManagerActivity extends AppCompatActivity {


    @OnClick(R.id.btnStart)
    public void start(){
        Intent i = new Intent(this, ToastService.class);
        this.startService(i);
    }

    @OnClick(R.id.btnStop)
    public void stop(){
        Intent i = new Intent(this, ToastService.class);
        this.stopService(i);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serwis_manager);
        ButterKnife.bind(this);


    }
}
