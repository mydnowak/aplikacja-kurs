package nowak.dariusz.aplikacjakurs;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PanstwaActivity extends AppCompatActivity {

        @BindView(R.id.tekstPanstwa) TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panstwa);
        ButterKnife.bind(this);


        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        // ustaw toolbar jako actionbar
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);

        String[] panstwa = {"Polska", "Francja", "Włochy", "Szkocja", "Szwecja", "Rosja", "Czechy", "Usa", "Australia", "Belgia", "Irlandia", "Portugalia", "Hiszpania", "Anglia", "Brazylia", "Argentyna"};

        ArrayList<String> panstwaL = new ArrayList<String>();
        panstwaL.addAll(Arrays.asList(panstwa));

        String text = "";
        for(String item : panstwaL){
            text = text + item + "<br/>";
        }
        txt.setText(NaszeMetody.fromHtml(text));


    }


}
