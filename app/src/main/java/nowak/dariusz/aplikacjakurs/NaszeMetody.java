package nowak.dariusz.aplikacjakurs;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * Created by dariusznowak on 12.06.2017.
 */

public class NaszeMetody {

    public static final String LOG_TAG = "DarekApp";
    public static Context AppContext;

    public static void InicjalizacjaNaszeMetody(Context appContext){
        AppContext = appContext;
    }

    public static void PrintLog(String s){
        Log.d(LOG_TAG, s);
    }
    public static void ShowMessage(String msg){
        Toast.makeText(AppContext, msg, Toast.LENGTH_SHORT).show();
    }

    public static void ShowSnackBar(View view, String msg){
        Snackbar mySnackbar = Snackbar.make(view,  msg, Snackbar.LENGTH_SHORT);
        mySnackbar.show();
    }
    public static String returnImageURI(int link){
        // aby otworzyc z resource bitmapy za pomoca ImageLoadera korzystam z wlasnej funkcji
        // ktora do nazwy zasobow dodaje przedrostek drawable://
        String imageUri = "drawable://" + link;
        return imageUri;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
