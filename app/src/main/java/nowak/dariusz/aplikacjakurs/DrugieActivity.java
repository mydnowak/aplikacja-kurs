package nowak.dariusz.aplikacjakurs;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class DrugieActivity extends Activity {

    String przekazanyAdres;
    //WebView przegladarka;

    @BindView(R.id.tvOk) TextView tvOk;
    @BindView(R.id.webView) WebView przegladarka;
    // BINDVIEW -- > ( ID z XML ) [ KONTROLKA ] (nazwa wlasna)


    @OnClick(R.id.btnHi)
    public void something(){
        tvOk.setText("Hi Android developer");
    }

    @OnClick(R.id.btnWWW)
    public void otworzStrone(){
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(przekazanyAdres));
        startActivity(i);
    }

    @OnLongClick(R.id.btnWWW)
    public boolean przejdz(){
        Log.d(TAG, "onLong!! ");

        Log.d(TAG, "a: "+ przekazanyAdres);

        // przegladarka laduje w oknie systemowym
        przegladarka.loadUrl(przekazanyAdres);
        return true;
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drugie);

        ButterKnife.bind(this);

        // odbior przekazanego paremtru
        Intent i = getIntent();

         przekazanyAdres = i.getStringExtra("dana_kod");

        // zamiast ponizszego kodu uzyty ButterKnife
        //TextView tvOk = (TextView) findViewById(R.id.tvOk);


        // ustawiamy tekst strony internetowej
        tvOk.setText(przekazanyAdres); 

//        Button btnPrzejdzDoWWW = (Button) findViewById(R.id.btnWWW);

//        btnPrzejdzDoWWW.setOnClickListener(onWebClick);

//        btnPrzejdzDoWWW.setOnLongClickListener(newLongWebClick);
        // przegladarka = (WebView) findViewById(R.id.webView);





    }
    final String TAG = "DarekApp";

    // listener ten to definicja reakcji na zdarenie w tym wypadku nacisniecie klikanego elementu
//    public View.OnClickListener onWebClick = new View.OnClickListener(){
//        public void onClick(View v){
//
//            // intecja nie jawna, wywolujemy otwarcie strony www przez zewnetrzny program
//            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(przekazanyAdres));
//            startActivity(i);
//        }
//    };

    // listener ten to definicja reakcji na zdarenie w tym wypadku nacisniecie dlugo (onLong) klikanego elementu
    public View.OnLongClickListener newLongWebClick = new View.OnLongClickListener(){
        public boolean onLongClick(View v){


            Log.d(TAG, "onLong!! ");
            Log.d(TAG, "a: "+ przekazanyAdres);

            // przegladarka laduje w oknie systemowym
            przegladarka.loadUrl(przekazanyAdres);
            // problem w systemowym androdizie ze jesli jest przekierowanie adresu to otworzy na zewnatrz
            // w celu ominiecia tego problemu trzeba wykorzystac inne metody


            return true;
        }
    };




}
