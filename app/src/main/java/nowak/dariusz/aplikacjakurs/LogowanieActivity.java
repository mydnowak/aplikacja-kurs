package nowak.dariusz.aplikacjakurs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogowanieActivity extends AppCompatActivity {

    @BindView(R.id.etPassword)
    EditText etPass;

    @BindView(R.id.etLogin)
    EditText etLogin;

    @BindView(R.id.etPasswordWrapper)
    TextInputLayout etPassWrapper;

    @BindView(R.id.etLoginWrapper)
    TextInputLayout etLoginWrapper;

    @OnClick(R.id.btnRejestruj)
    public void zarejestruj(){
        // przejscie za pomoca intencji
        Intent i = new Intent(LogowanieActivity.this, RegisterActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btnZaloguj)
    public void logowanie(){

        String email = etLogin.getText().toString();
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etLoginWrapper.setError(getString(R.string.poprawny_mail));
            return;
        }else{
            etLoginWrapper.setError(null);
        }
        if(etPass.getText().length()< 10){
            etPassWrapper.setError(getString(R.string.short_pass));
            return;
        }else{
            etPassWrapper.setError(null);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logowanie);
        ButterKnife.bind(this);

        //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences sharedPreferences = getSharedPreferences("dnowak.loginy", Context.MODE_PRIVATE);
        String login = sharedPreferences.getString(getString(R.string.PREF_LOGIN), "");
        String haslo = sharedPreferences.getString(getString(R.string.PREF_HASLO), "");

        etLogin.setText(login);
        etPass.setText(haslo);

    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
    }
}
