package nowak.dariusz.aplikacjakurs;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZdjecieActivity extends AppCompatActivity {

    @BindView(R.id.imageView)
    ImageView kontrolkaZdjecia;

    String TAG = "DarekApp";

    @OnClick(R.id.floatingActionButton)
    public void wczytajZdjecie(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ZdjecieActivity.this);
        alertDialog.setTitle("Wybierz opcję");

        final CharSequence[] items = {"Zrób zdjęcie" , "Wybierz z Galerii", "Wczytaj z internetu"};

        alertDialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0){
                    zrobZdjecie();
                }else if(which == 1){
                    wybierzGaleria();
                }else if(which == 2){
                    zaladujInternety();
                }
            }
        });

        alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog al = alertDialog.create();
        al.show();


    }

    public void zaladujInternety(){
//        // utworzenie instancji
//        ImageLoader imageLoader = ImageLoader.getInstance();
//
//        // wyswietlenie zdjecia ( link do zdjecia + przejscie do kontrolki )
//        PhotoSecretReturner fotkaBiblioteczna = new PhotoSecretReturner();
//        String f = fotkaBiblioteczna.zwrocZdjecieLista();
//        Log.d(TAG, f);
//        imageLoader.displayImage(f, kontrolkaZdjecia);
//        //imageLoader.displayImage("https://www.gatesms.eu/img/android.png", kontrolkaZdjecia);

        Glide.with(getApplicationContext()).load("https://betaimages.sunfrogshirts.com/2016/04/01/m_Awesome-Tee-For-Junior-Android-Developer-Red-_w91_-front.jpg").into(kontrolkaZdjecia);
    }

    private int PICK_IMAGE_REQUEST = 1;
    private int MAKE_IMAGE_REQUEST = 2;
    private int STORAGE_PERMISSION_CODE = 123;


    public void wybierzGaleria(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showExplanation();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
            }
            return;
        }else{
            wczytajGaleria();
        }
    }

    private void showExplanation() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("Potrzebujemy pozwolenia")
                .setMessage("Aby móc załadować zdjęcie, potrzebujemy dostep do galerii")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActivityCompat.requestPermissions(ZdjecieActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                    }
                });
        builder.create().show();
    }
    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                wybierzGaleria();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Nie będziemy mogli wykonać tej operacji", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void wczytajGaleria(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Wybierz zdjęcie"), PICK_IMAGE_REQUEST);
    }

    public void zrobZdjecie(){
        Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePhoto.resolveActivity(getPackageManager()) != null){
            startActivityForResult(takePhoto, MAKE_IMAGE_REQUEST);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zdjecie);

        ButterKnife.bind(this);

        if(savedInstanceState != null){
            sciezkaZdjecia = savedInstanceState.getString(IMAGE);
            Glide.with(getApplicationContext()).load(sciezkaZdjecia).into(kontrolkaZdjecia);
        }
    }

    String sciezkaZdjecia = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == PICK_IMAGE_REQUEST) {
                Uri filePath = data.getData();
                String log2 = getRealPathFromUri(getApplicationContext(), filePath);
                sciezkaZdjecia = log2;
                Glide.with(getApplicationContext()).load("file://"+sciezkaZdjecia).into(kontrolkaZdjecia);

            } else if(requestCode == MAKE_IMAGE_REQUEST){
                String photoPath = "";

                Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED, MediaStore.Images.ImageColumns.ORIENTATION}, MediaStore.Images.Media.DATE_ADDED, null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");
                if (cursor != null && cursor.moveToFirst()) {

                        cursor.moveToFirst();
                        Uri uri = Uri.parse(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)));
                        photoPath = uri.toString();
                        cursor.close();
                }
                sciezkaZdjecia = photoPath;
                Glide.with(getApplicationContext()).load("file://"+sciezkaZdjecia).into(kontrolkaZdjecia);

            }
        }

    }



    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    static final String IMAGE = "image";
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(IMAGE, sciezkaZdjecia);
        super.onSaveInstanceState(outState);
    }

}
