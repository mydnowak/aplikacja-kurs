package nowak.dariusz.aplikacjakurs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.LinearLayout;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import nowak.dariusz.dane.ZapytaniaBazy;

public class ToDoListActivity extends AppCompatActivity implements onWyborZmianaListener {

    // optional oznacza ze nie musi byc floating action button na ekranie co jest prawda bo nie ma go w trybie landscape
    @Optional
    @OnClick(R.id.floatingActionButton)
    public void dodaj(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ToDoListActivity.this);
        alertDialog.setTitle("Wpisz zadanie do wykonania");

        LinearLayout lay = new LinearLayout(this);

        lay.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        final EditText tekstGlowny = new EditText(this);
        tekstGlowny.setHint("Wpisz nazwe zadania");
        tekstGlowny.setLayoutParams(lp);
        final EditText opisZadania = new EditText(this);
        opisZadania.setHint("Wpisz opis zadania");
        opisZadania.setLayoutParams(lp);
        lay.addView(tekstGlowny);
        lay.addView(opisZadania);
        alertDialog.setView(lay);

        alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String zwrotIdWprowadzonego = polaczenieBazy.dodajToDoBazy(tekstGlowny.getText().toString(), opisZadania.getText().toString(), 0);
                ToDoListaFragment fr = (ToDoListaFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentLista);
                // wykonuje metode we fragmencie ktora odswiezy adapter
                fr.odswiezAdapter();
            }
        });

        alertDialog.show();


//        String zwrotIdWprowadzonego = polaczenieBazy.dodajToDoBazy("")
//        wyswietlacz.setText(zwrotIdWprowadzonego);
    }

    ZapytaniaBazy polaczenieBazy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);
        ButterKnife.bind(this);

        polaczenieBazy = new ZapytaniaBazy(ToDoListActivity.this);
        polaczenieBazy.otworzZapis();
    }

    // za pomoca tego interfejsu operujemy na przekazanym tekscie
    @Override
    public void onZmianaListener(String text) {

        // sprawdzamy jaka jest konfiguracja ekranu
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                // jesli ekran jest portait to wysylamy do drugiej intecji
            Intent i = new Intent(this, ToDoSzczegolyActivity.class);
            i.putExtra("szcz", text);
            startActivity(i);
        }else{
            // jesli ekran jest landscape lub inny to odnajdujemy za pomoca findfragmentbyid fragment ze szczegolami
            // i tam wywolujemy metode ktora ustawia tekst
            ToDoSzczegolyFragment fr = (ToDoSzczegolyFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentSzcz);
            fr.ustawTekst(text);
        }
    }
}
