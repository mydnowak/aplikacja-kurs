package nowak.dariusz.aplikacjakurs;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.greenrobot.greendao.database.Database;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nowak.dariusz.aplikacjakurs.db.DaoMaster;
import nowak.dariusz.aplikacjakurs.db.DaoSession;
import nowak.dariusz.aplikacjakurs.db.Produkt;
import nowak.dariusz.aplikacjakurs.db.ProduktDao;

public class SMSActivity extends AppCompatActivity {

    EditText numerTelefonu;
    EditText tekstWiadomosci;
    Button btnSMS;
    Button btnObliczeniowy;

    // stala dzieki ktorej po odpowiedzi od aplikacji o statusie przyznania dostepu bedziemy
    // wiedziec ze jest to odpowiedz na dane nasze wywolanei
    final int MY_PERMISSIONS_REQUEST_WRITE_SMS = 1;
    final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 2;
    final int MY_PERMISSIONS_REQUEST_READ_SMS = 3;

    // nasz TAG za pomoca ktorego bedziemy mogli filtrowac wiadomosci w Android Monitor
    public static final String TAG = "DarekApp";

    CoordinatorLayout layoutSms;
    ListView smsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        numerTelefonu = (EditText) findViewById(R.id.numerTelefonu);
        tekstWiadomosci = (EditText) findViewById(R.id.textWiadomosci);

        layoutSms = (CoordinatorLayout) findViewById(R.id.layout_sms);

        smsListView = (ListView) findViewById(R.id.smsRecyclerView);
        smsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // odczytuje klikniety numer telefonu
                String numerTel= (String) parent.getItemAtPosition(position);
                // wywoluje funkcje ktora zwraca liste smsow dla tego unikalnego numeru
                 List<String> listaSmsow = przeczytajSmsDlaNumer(numerTel);

                // laduje adapter
                smsListView.setAdapter(new ArrayAdapter<String>(SMSActivity.this, android.R.layout.simple_list_item_1, listaSmsow));

                // ustawiam numer telefonu wybrany
                numerTelefonu.setText(numerTel);
            }
        });

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSMSClick();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSms);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Manager");

        // odczytuje po przejsciu intenta
        Intent i = getIntent();
        String suma = i.getStringExtra("suma");
        tekstWiadomosci.setText("Na zakupy będę potrzebował: " + suma);


    }

    // aby utworzyc menu z pliku xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_sms, menu);
        return true;
    }

    // aby przypisac akcje do elementow w menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.idCall:
                 onPhoneClick();
                return true;
            case R.id.idRestaurant:
                wyslijMenuSMS();
                return true;
            case R.id.idMinus:
                odczytaj();
                return true;
            case R.id.idPlus:
                zapisz();
                return true;
            case R.id.idReadSms:
                onReadSms();
                return true;
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void odczytaj(){

        FileInputStream fis = null;
        try {
            fis =  getApplicationContext().openFileInput(filename);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine()) != null){
                sb.append(line);
            }
            numerTelefonu.setText(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Snackbar.make(layoutSms, "Odczytane", Snackbar.LENGTH_SHORT).show();
    }

    String filename = "mojPlik";

    public void zapisz(){

        String doZapisu = numerTelefonu.getText().toString();
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(doZapisu.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        Snackbar.make(layoutSms, "Zapisane", Snackbar.LENGTH_SHORT).show();
    }

    Database db;
    private DaoSession daoSession;
    ProduktDao produktDao;

    List<Produkt> menu;

    public void polaczBaz(){
        // otwieramy polaczenie z baza
        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(this, "users-db");
        db = pomocnik.getWritableDb();
        // tworzymy nowa sesje
        daoSession = new DaoMaster(db).newSession();
        // laczymy sie z produktDao
        produktDao = daoSession.getProduktDao();
        menu = produktDao.queryBuilder().list();
    }

    public void wyslijMenuSMS(){
        polaczBaz();

      //  startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", "111", null)).putExtra("sms_body", zrobListe()));


       sendSms(numerTelefonu.getText().toString(), zrobListe());
    }



    public String zrobListe(){
        String msg = "";
        for(Produkt item : menu){
            msg = msg.concat(" " + item.getNazwa() + "\n");
        }

        return msg;
    }

    // powoduje ze nie dzownimy auotmatycznie a jedynie wyruzca na ekran dzownienia numer telefonu
    // dzieki czemu nie potrzebujemy pozwolenia
    private void wykonajFakeTelefon() {
        String telString = "tel:" + numerTelefonu.getText().toString();
        Intent i = new Intent(Intent.ACTION_DIAL);
        i.setData(Uri.parse(telString));
        startActivity(i);
    }



    public void wykonajTelefon(){
        String telString = "tel:" + numerTelefonu.getText().toString();
        Intent i = new Intent(Intent.ACTION_CALL);
        i.setData(Uri.parse(telString));
        startActivity(i);
    }




    // wlasna funkcja ktora przyjmuje numer telefonu i tresc wiadomosci
    private void sendSms(String phoneNo, String msg) {
        try {
            //wykorzystujemy smsmanager czyli wbudowane api do zarzadzania smsami
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);

            // za pomoca Log mozemy zrobic log momencie wyslania SMS . Ten log jest widoczny w ANdroid Monitorze.
            Log.d(TAG, "SMS Wysłany");

            // wyslanie snackabara
//            Snackbar mySnackbar = Snackbar.make(layoutSms, "Sms Wysłany", Snackbar.LENGTH_SHORT);
//            mySnackbar.show();
            NaszeMetody.PrintLog("SMS WYSLANY");
//            NaszeMetody.ShowMessage("NO WITAM");
            NaszeMetody.ShowSnackBar(layoutSms, "SMS WYSLANY");

            // ponizej na dwa sposoby czyscimy wpisane kontrolki
            numerTelefonu.setText("");
            tekstWiadomosci.getText().clear();


            // ustawiam result i zamykam activity
            setResult(RESULT_OK);
            finish();

        } catch (Exception ex) {
            Log.d(TAG, "SMS Nie wysłany");
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }



    public void onSMSClick(){

            // sprawdzamy czy jest przyznay dostep
            if(android.support.v4.app.ActivityCompat.checkSelfPermission(SMSActivity.this, Manifest.permission.SEND_SMS)
                    != PackageManager.PERMISSION_GRANTED){

                // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
                if (ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                        Manifest.permission.SEND_SMS)){
                    // jesli dostep blokowal pokazujemy po co nam to potrzebne
                    showExplanation("Potrzebujemy pozwolenia", "Chcemy wysłać SMS który napisałeś, więc potrzebujemy pozwolenia",
                            Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
                }else{
                    // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                    requestPermissions(Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
                }
            }else{
                 // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
                // sendsms to wlasna funkcja do wysylania smsow opisana ponizej
                sendSms(numerTelefonu.getText().toString(), tekstWiadomosci.getText().toString());

            }
        }

    // funkcja ktora pokazuje okienko systemowe z prosba o dany kod
    private void requestPermissions(String permissionName, int permissionRequestCode){
       ActivityCompat.requestPermissions(this, new String[]{permissionName} , permissionRequestCode);
    }

    // wlasna funkcja ktora pokazuje okienko z wyjasnieniem prosby o dostep
    private void showExplanation(String title, String message, final String permission, final int permissionRequestCode){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                requestPermissions(permission, permissionRequestCode);
            }
        });
        builder.show();
    }


    // metoda wywolywana za kazdym razem gdy uzytkownik podejmie decyzje o dostepie
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_SMS: {
                // jesli uzytkownik dal anuluj to dlugosc listy bedzie pusta
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendSms(numerTelefonu.getText().toString(), tekstWiadomosci.getText().toString());
                    // dostep przyznany - mozemy zrobic co chcemy
                    Log.d(TAG, "Dostęp przyznany");
                } else {
                    Log.d(TAG, "Dostęp nie przyznany");
                    //  dostep nie przyznany ! musimy obsluzyc ten problem w aplikacji
                    // ponizej dodatkowo sprawdzamy czy zaznaczyl never ask again
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                                Manifest.permission.SEND_SMS);
                        if (!showRationale) {
                            Log.d(TAG, "Uzytkownik zaznaczyl never ask again");
                        }
                    }
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        wykonajTelefon();
                    // dostep przyznany - mozemy zrobic co chcemy
                    Log.d(TAG, "Dostęp przyznany");
                } else {
                Snackbar mySnackbar = Snackbar.make(layoutSms, "Dostęp nie przyznany", Snackbar.LENGTH_SHORT);
            mySnackbar.show();
                    //  dostep nie przyznany ! musimy obsluzyc ten problem w aplikacji
                    // ponizej dodatkowo sprawdzamy czy zaznaczyl never ask again
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                                Manifest.permission.CALL_PHONE);
                        if (!showRationale) {
                            Log.d(TAG, "Uzytkownik zaznaczyl never ask again");
                        }
                    }
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_READ_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pobierzSMSy();
                    // dostep przyznany - mozemy zrobic co chcemy
                    Log.d(TAG, "Dostęp przyznany");
                } else {
                    Snackbar mySnackbar = Snackbar.make(layoutSms, "Dostęp nie przyznany", Snackbar.LENGTH_SHORT);
                    mySnackbar.show();
                    //  dostep nie przyznany ! musimy obsluzyc ten problem w aplikacji
                    // ponizej dodatkowo sprawdzamy czy zaznaczyl never ask again
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                                Manifest.permission.READ_SMS);
                        if (!showRationale) {
                            Log.d(TAG, "Uzytkownik zaznaczyl never ask again");
                        }
                    }
                }
                return;
            }

            // za pomoca swticha mozna przejrzec czasmi wiele prosb
        }
    }

    public void onPhoneClick(){

        // sprawdzamy czy jest przyznay dostep
        if(android.support.v4.app.ActivityCompat.checkSelfPermission(SMSActivity.this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED){

            // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
            if (ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                    Manifest.permission.CALL_PHONE)){
                // jesli dostep blokowal pokazujemy po co nam to potrzebne
                showExplanation("Potrzebujemy pozwolenia", "Chcemy zadzwonić do osoby, której numer podałeś, więc potrzebujemy pozwolenia",
                        Manifest.permission.CALL_PHONE, MY_PERMISSIONS_REQUEST_CALL_PHONE);
            }else{
                // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                requestPermissions(Manifest.permission.CALL_PHONE, MY_PERMISSIONS_REQUEST_CALL_PHONE);
            }
        }else{
            // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
            // sendsms to wlasna funkcja do wysylania smsow opisana ponizej
       wykonajTelefon();
        }
    }

    public void onReadSms(){

        // sprawdzamy czy jest przyznay dostep
        if(android.support.v4.app.ActivityCompat.checkSelfPermission(SMSActivity.this, Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED){

            // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
            if (ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                    Manifest.permission.READ_SMS)){
                // jesli dostep blokowal pokazujemy po co nam to potrzebne
                showExplanation("Potrzebujemy pozwolenia", "Chcemy odczytać SMS który napisałeś, więc potrzebujemy pozwolenia",
                        Manifest.permission.READ_SMS, MY_PERMISSIONS_REQUEST_READ_SMS);
            }else{
                // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                requestPermissions(Manifest.permission.READ_SMS, MY_PERMISSIONS_REQUEST_READ_SMS);
            }
        }else{
            // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
            pobierzSMSy();
        }
    }


    public void pobierzSMSy(){
            List<String> smsy = readSms();

            Set<String> unikalneSmsy = new HashSet<String>(smsy);
            smsy.clear();
            smsy.addAll(unikalneSmsy);
            smsListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, smsy));

        // wyswietlam ilosc elementow na lisce
            numerTelefonu.setText(String.valueOf(smsy.size()));
    }

    public List<String> przeczytajSmsDlaNumer(String numer){
        List<String> smsy = new ArrayList<>();
        Uri smsyInbox = Uri.parse("content://sms/inbox");

        String[] kolumnyKtoreChcemyWyswietlic = new String[] {"body"};
        String warunekWhere = "address='"+ numer + "'";
        Cursor cursor = getContentResolver().query(smsyInbox, kolumnyKtoreChcemyWyswietlic, warunekWhere, null, null);
        // select KolumnaBody From Smsy/Inbox Where Address = podanyNumerTelefonu

        // znajdujemy indexKolumny
        int indexBody = cursor.getColumnIndex("body");

        while(cursor.moveToNext()){
            // pobieram kolumne o indexie 2 w ktorej sa informacje o adresacie
            String trescSms = cursor.getString(indexBody);
            smsy.add(trescSms);

        }
        cursor.close();

        return smsy;

    }


    public List<String> readSms(){
        List<String> adresaci = new ArrayList<>();
        Uri smsyInbox = Uri.parse("content://sms/inbox");

        Cursor cursor = getContentResolver().query(smsyInbox, null, null, null, null);

        int indexAdres = cursor.getColumnIndex("address");

        while(cursor.moveToNext()){
            // pobieram kolumne o indexie 2 w ktorej sa informacje o adresacie
            String adresat = cursor.getString(indexAdres);
            adresaci.add(adresat);

            // zakomentowany kod pozwala do jednego stringa wsadzic wszystkie mozliwe kolumny z danymi z jednego wiersza kursora
//            for(int idx=0;idx<cursor.getColumnCount();idx++)
//            {
//                String msgData = " " + cursor.getColumnName(idx) + ":" + cursor.getString(idx);
//                adresaci.add(msgData);
//            }

        }

        return adresaci;
    }


}
