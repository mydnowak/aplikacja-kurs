package nowak.dariusz.aplikacjakurs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import nowak.dariusz.dane.ToDoItem;
import nowak.dariusz.dane.ZapytaniaBazy;


/**
 * A simple {@link Fragment} subclass.
 */
public class ToDoListaFragment extends Fragment {


    public ToDoListaFragment() {
        // Required empty public constructor
    }

    RecyclerView mojRecycler;
    List<ToDoItem> listaZadan;
    ToDoAdapter pa;
    ZapytaniaBazy polaczenieBazy;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_to_do_lista, container, false);


        mojRecycler = (RecyclerView) v.findViewById(R.id.mojRVi);

        return v;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        polaczenieBazy = new ZapytaniaBazy(getActivity());
        polaczenieBazy.otworzOdczyt();

        listaZadan = polaczenieBazy.zwrocListeToDo();


        // wybieramy manager layoutu (dostepny jeszcze Grid i StabbedGrid
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        // ustawiamy Manager Layoutu
        mojRecycler.setLayoutManager(llm);

        //utworzneie odstepu pomiedzy elementami listy
        DividerItemDecoration podzielenie = new DividerItemDecoration(mojRecycler.getContext(), llm.getOrientation());
        mojRecycler.addItemDecoration(podzielenie);

        // ustwaiamy nasz wlasny adapter do recyclerview
        pa = new ToDoAdapter(listaZadan, getActivity());
        mojRecycler.setAdapter(pa);
    }

    public void odswiezAdapter(){
        listaZadan = polaczenieBazy.zwrocListeToDo();
        pa = new ToDoAdapter(listaZadan, getActivity());
        mojRecycler.setAdapter(pa);
    }

}
