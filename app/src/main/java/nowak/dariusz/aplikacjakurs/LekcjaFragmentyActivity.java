package nowak.dariusz.aplikacjakurs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LekcjaFragmentyActivity extends AppCompatActivity implements DrugiFragment.PrzesylanyTekst {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lekcja_fragmenty);
        ButterKnife.bind(this);


    }

    @OnClick(R.id.btnPierwszyFr)
    public void wczytajPierwszyFragment(){
        zaladujFragment(new PierwszyFragment());
    }

    @OnClick(R.id.btnDrugiFr)
    public void wczytajDrugiFragment(){
        DrugiFragment drugiFragment = new DrugiFragment();
        // utworzony obiekt bundle do przekazania
        Bundle bundle = new Bundle();
        bundle.putString("tajne", "Tajny Kod to: 20-07-2017");
        // wkladamy do fragmentu
        drugiFragment.setArguments(bundle);
        zaladujFragment(drugiFragment);
    }


    // laduje fragmenty
    public void zaladujFragment(Fragment fragment){

        // jesli biblioteka support to getSupportFragmentManager , a jesli nie to getFragmentManager
        FragmentManager fm = getSupportFragmentManager();
        // otwieramy transakcje
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // zastepujemy fragment aktualny , fragmentem ktory wkladamy do FrameLayout
        fragmentTransaction.replace(R.id.kontenerDlaFragmentow, fragment);
        // dodanie do stosu
        fragmentTransaction.addToBackStack(null);
        // potwierdamy transakcje
        fragmentTransaction.commit();

    }

    @Override
    public void przeslijText(String text) {
        // pomysl 1 - uruchom automatycznie nowy fragment
        PierwszyFragment  pierwszyFragment = new PierwszyFragment();
        Bundle bundle = new Bundle();
        bundle.putString("tajne", text);
        pierwszyFragment.setArguments(bundle);
        zaladujFragment(pierwszyFragment);

        // pomysl 2 - ustawic wiadomosc pobrana na zewntarz i uruchamiac na przycisk
        // pomysl 3 - jesli obydwa fragmenty sa na ekranie to mozna wywolac metode z fragmentu poprzez activity automatycznie


    }

    @Override
    public void onBackPressed() {
        // sprawdza czy na stosie jest cokolwiek jesli tak to cofa po stosie o 1,a jesli nie to wychodzi z activity
        if(getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportFragmentManager().popBackStack();
        }else{
            super.onBackPressed();
        }
    }
}
