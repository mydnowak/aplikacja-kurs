package nowak.dariusz.dane;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by dariusznowak on 18.07.2017.
 */

public class FilmContentProvider extends ContentProvider {

    private PomocnikBazy pomocnikBazy;
    public static final String SCIEZKA_BAZOWA = "/filmy";

    public static final String AUTHORITY = "dariusznowak.filmy";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + SCIEZKA_BAZOWA);

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    public static final int WYBRANY_WIELE_FILMOW = 1;
    public static final int WYBRANY_JEDEN_FILM = 2;

    static{
        sUriMatcher.addURI(AUTHORITY, SCIEZKA_BAZOWA, WYBRANY_WIELE_FILMOW);
        sUriMatcher.addURI(AUTHORITY, SCIEZKA_BAZOWA+ "/#", WYBRANY_JEDEN_FILM);
    }

    @Override
    public boolean onCreate() {
        pomocnikBazy = new PomocnikBazy(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        // uzyjemy SQLiteQueryBuilder zamiast metody query()
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // sprawdzamy czy sa ktos wybral kolumny ktorych nie ma
        sprawdzKolumny(projection);

        // ustaw tabele
        queryBuilder.setTables(FilmyContract.FilmTabela.NAZWA_TABELI);

        // sprawdzi nam ktore URI wybral uzytkownik czyli czy wpisal ze chce pobrac jeden film czy wiele
        int uriType = sUriMatcher.match(uri);
        switch (uriType){
            case WYBRANY_WIELE_FILMOW:
                break;
            case WYBRANY_JEDEN_FILM:
                // dodanie id do zapytania
                queryBuilder.appendWhere(FilmyContract.FilmTabela._ID + "="
                        + uri.getLastPathSegment());
                break;
            default:
                // wyrzucamy wyjatek jesli ktos wpisze inne URI ktore nie bedzie pasowac do zadnego z osobna
                throw new IllegalArgumentException("Nieznane URI:" + uri);
        }

        SQLiteDatabase db = pomocnikBazy.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // ewentualnych nasluchaczy poinformuj o zmianach
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase sqlDB = pomocnikBazy.getWritableDatabase();
        long id = 0;
        switch (uriType) {
            case WYBRANY_WIELE_FILMOW:
                id = sqlDB.insert(FilmyContract.FilmTabela.NAZWA_TABELI, null, values);
                break;
            default:
                throw new IllegalArgumentException("Nieznane URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(SCIEZKA_BAZOWA + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase sqlDB = pomocnikBazy.getWritableDatabase();
        int rowsDeleted = 0;
        switch (uriType) {
            case WYBRANY_WIELE_FILMOW:
                rowsDeleted = sqlDB.delete(FilmyContract.FilmTabela.NAZWA_TABELI, selection,
                        selectionArgs);
                break;
            case WYBRANY_JEDEN_FILM:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(
                            FilmyContract.FilmTabela.NAZWA_TABELI,
                            FilmyContract.FilmTabela._ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(
                            FilmyContract.FilmTabela.NAZWA_TABELI,
                            FilmyContract.FilmTabela._ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Nieznane URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase sqlDB = pomocnikBazy.getWritableDatabase();
        int rowsUpdated = 0;
        switch (uriType) {
            case WYBRANY_WIELE_FILMOW:
                rowsUpdated = sqlDB.update(FilmyContract.FilmTabela.NAZWA_TABELI,
                        values,
                        selection,
                        selectionArgs);
                break;
            case WYBRANY_JEDEN_FILM:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(FilmyContract.FilmTabela.NAZWA_TABELI,
                            values,
                            FilmyContract.FilmTabela._ID  + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(FilmyContract.FilmTabela.NAZWA_TABELI,
                            values,
                            FilmyContract.FilmTabela._ID  + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Nieznae URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private void sprawdzKolumny(String[] projection) {
        String[] dostepne = {FilmyContract.FilmTabela.KOLUMNA_TYTUL, FilmyContract.FilmTabela.KOLUMNA_OCENA, FilmyContract.FilmTabela._ID,
                FilmyContract.FilmTabela.KOLUMNA_ROK_POWSTANIA, FilmyContract.FilmTabela.KOLUMNA_BUDZET};
        if (projection != null) {
            HashSet<String> pozadaneKolumny = new HashSet<String>(
                    Arrays.asList(projection));
            HashSet<String> dostepneKolumny = new HashSet<String>(
                    Arrays.asList(dostepne));
            // Sprawdz czy wszystkie kolumny o ktore proszono sa dostepne
            if (!dostepneKolumny.containsAll(pozadaneKolumny)) {
                throw new IllegalArgumentException(
                        "Nieznane kolumny w projection");
            }
        }
    }

    // aby odnalezc sie w innej aplikacji mozna to zrobic w ten sposob:
   /**
    List<String> listaFilmow = new ArrayList<>();
    Uri filmy = Uri.parse("content://dariusznowak.filmy/filmy");

    Cursor cursor = getContentResolver().query(filmy, null, null, null, null);

    int indexTytul = cursor.getColumnIndex("tytul");

        while(cursor.moveToNext()){
        // pobieram kolumne o indexie 2 w ktorej sa informacje o adresacie
        String tytul = cursor.getString(indexTytul);
        listaFilmow.add(tytul);

    }
    **/

}
