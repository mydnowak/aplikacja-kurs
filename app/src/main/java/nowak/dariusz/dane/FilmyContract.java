package nowak.dariusz.dane;

import android.provider.BaseColumns;

/**
 * Created by dariusznowak on 04.07.2017.
 */

public final class FilmyContract {

    private FilmyContract() {}

    // opisujemy jak wygladaja tabele i kolumny
    public static class FilmTabela implements BaseColumns{
        public static final String NAZWA_TABELI = "Film";
        public static final String KOLUMNA_TYTUL = "tytul";
        public static final String KOLUMNA_BUDZET = "budzet";
        public static final String KOLUMNA_ROK_POWSTANIA = "rokPowstania";
        public static final String KOLUMNA_OCENA = "ocena";
        // psfs + enter (tajny kod seniora)
    }


    // opisujemy jak wygladaja tabele i kolumny
    public static class ToDoTabela implements BaseColumns{
        public static final String NAZWA_TABELI = "ToDo";
        public static final String KOLUMNA_NAZWA_ZADANIA = "zadanie";
        public static final String KOLUMNA_CZY_ZROBIONE = "zrobione";
        public static final String KOLUMNA_SZCZEGOLY = "szczegoly";
        // psfs + enter (tajny kod seniora)
    }



}
