package nowak.dariusz.dane;

/**
 * Created by dariusznowak on 06.07.2017.
 */

public class ToDoItem {
    private int id;
    private String nazwaZadania;
    private int zrobione;
    private String opisZadania;

    public ToDoItem(int id, String nazwaZadania, int zrobione, String opisZadania) {
        this.id = id;
        this.nazwaZadania = nazwaZadania;
        this.zrobione = zrobione;
        this.opisZadania = opisZadania;
    }

    public ToDoItem() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwaZadania() {
        return nazwaZadania;
    }

    public void setNazwaZadania(String nazwaZadania) {
        this.nazwaZadania = nazwaZadania;
    }

    public int getZrobione() {
        return zrobione;
    }

    public void setZrobione(int zrobione) {
        this.zrobione = zrobione;
    }

    public String getOpisZadania() {
        return opisZadania;
    }

    public void setOpisZadania(String opisZadania) {
        this.opisZadania = opisZadania;
    }
}
