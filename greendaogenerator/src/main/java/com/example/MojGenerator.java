package com.example;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;

public class MojGenerator {
    public static void main(String[] args) {
        Schema schema = new Schema(4, "nowak.dariusz.aplikacjakurs.db");
        schema.enableKeepSectionsByDefault();

        AddTables(schema);

        try
        {
            new DaoGenerator().generateAll(schema, "./app/src/main/java");
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    // ta funkcja dodaje schematy tabelek bazy danych
    private static void AddTables(Schema schema) {
//       addProduktEntity(schema);
//        addZamowieniaEntity(schema);

        Entity produkt = schema.addEntity("Produkt");
        produkt.addIdProperty().primaryKey().autoincrement();
        produkt.addStringProperty("nazwa");
        produkt.addFloatProperty("cena");

        Entity koszyk = schema.addEntity("Koszyk");
        koszyk.addIdProperty().primaryKey().autoincrement();
        Property produktIdProperty = koszyk.addLongProperty("produktId").getProperty();
        koszyk.addToOne(produkt, produktIdProperty);
        koszyk.addIntProperty("ilosc");

        Entity zamowienia = schema.addEntity("Zamowienia");
        zamowienia.addIdProperty().primaryKey().autoincrement();
        zamowienia.addFloatProperty("suma");
        zamowienia.addDateProperty("data");
        zamowienia.addStringProperty("imie");
        zamowienia.addStringProperty("nazwisko");
        zamowienia.addStringProperty("ulica");
        zamowienia.addStringProperty("kodPocztowy");
        zamowienia.addStringProperty("miasto");

        Entity szczegolyZamowienia = schema.addEntity("SzczegolyZamowienia");
        szczegolyZamowienia.addIntProperty("ilosc");
        szczegolyZamowienia.addIdProperty().primaryKey().autoincrement();

        Property produktIdPropertyDlaSzczegoly = szczegolyZamowienia.addLongProperty("produktId").getProperty();
        szczegolyZamowienia.addToOne(produkt, produktIdPropertyDlaSzczegoly);
        Property zamowienieIdPropertyDlaSzczegoly = szczegolyZamowienia.addLongProperty("zamowienieId").getProperty();
        szczegolyZamowienia.addToOne(zamowienia, zamowienieIdPropertyDlaSzczegoly);
    }



}
