package nowak.dariusz.secretphoto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by dariusznowak on 25.05.2017.
 */

public class PhotoSecretReturner {

    // utworzyc metode ktora zwraca stringa z adresem url do zdjecia

    public String zwrocZdjecie(){
        return "https://www.android.com/static/2016/img/hero-carousel/android-nougat.png";
    }


    // * utowrzyc liste zdjec z ktorej losowne jest zwracany string z adreselem url do zdjecia

    ArrayList<String> listaZdjec = new ArrayList<String>(Arrays.asList("https://www.android.com/static/2016/img/hero-carousel/android-nougat.png",
            "http://wearableweb.pl/wp-content/uploads/2016/02/Find-your-phone-with-Android-Wear.png","https://www.androidcentral.com/sites/androidcentral.com/files/styles/large/public/topic_images/2014/google-glass.jpg"));

    ArrayList<String> listaZdjecDruga = new ArrayList<>();

   public PhotoSecretReturner(){
       listaZdjecDruga.add("https://www.android.com/static/2016/img/hero-carousel/android-nougat.png");
       listaZdjecDruga.add("http://geeknizer.com/wp-content/uploads/2013/05/google-glass-android-apps-apk.jpg");
       listaZdjecDruga.add("http://www.wpa.org.pl/wp-content/uploads/2015/03/android-wear.jpg");
   }

   public String zwrocZdjecieLista(){
       Random los = new Random();
       int wylosowanyIndex = los.nextInt(listaZdjec.size());
       return listaZdjec.get(wylosowanyIndex);
   }

    public String zwrocZdjecieListaDruga(){
        Random los = new Random();
        int wylosowanyIndex = los.nextInt(listaZdjecDruga.size());
        return listaZdjecDruga.get(wylosowanyIndex);
    }


    // ** zabezpiecz przed wystepowaniem dwukrotnie tego samego zdjecia



}
